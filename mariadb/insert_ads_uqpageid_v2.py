#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
# import pandas as pd
# import numpy as np
import pymysql.cursors
import logging
import multiprocessing as mp

# import json
# import re
# import hashlib # decided to use pagid instead 
# v1, from ads_2018March to another table. a narrow down process.
# v2, MariaDB's spec is too weak. 4 CPU/15.7G RAM
# query from ELK and store to Dict then to SQL
# store to sql as unique pageid and keyword

argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')
startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]
indextb = argvs.index('-tb')
tablename = argvs[indextb+1]
indexproc = argvs.index('-proc')
numprocess = int(argvs[indexproc+1])
indexesidx = argvs.index('-esidx')
esidx = argvs[indexesidx+1]
indexestype = argvs.index('-estype')
estype = argvs[indexestype+1]
# indexaggsize = argvs.index('-aggsize')
# aggs_size = argvs[indexaggsize+1]


filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


logger.info('Start!')

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	outstr = ','.join(outlist)	
	return outstr

def toDateObj(indate):
	# from 20180301 to 2018-03-01
	return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))
'''=================== self-defined functions end ==================='''


start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
step = delta//numprocess



def worker(stepnum):

	worker_start = start + step*stepnum
	if stepnum == numprocess-1:
		worker_end = end
	else:
		worker_end = worker_start + step - timedelta(days=1)

	body = {}
	body['_source'] = ["pageid", "ads_keyword", "ads"]
	# body['size'] = 0
	body['query'] = {}
	body['query']['bool'] = {}
	body['query']['bool']['must'] = []
	if '-zi' in sys.argv: # params for zi.media only
		body['query']['bool']['must'].append({"term": {"hostname": "zi.media"}})
	body['query']['bool']['must'].append({"range": { "updated": {"gte": str(worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})	

	logger.info(body)

	esCluster = Elasticsearch(
	    # hosts = ['192.168.21.20'],
	    hosts = ['192.168.21.18'],
	    http_auth = ('elastic', 'breaktime168'),
	    port = 9200,
	    timeout = 36000
	)
	res = helpers.scan(
					client = esCluster,
					scroll = '20m',
					size = 10000,
					query = body,
					index = esidx,
					doc_type= estype,
					request_timeout = 5000
	)	
	# res = esCluster.search(
	# 	index=esidx,
	# 	doc_type=estype,
	# 	# scroll="3m",
	# 	search_type="query_then_fetch",
	# 	# size=scroll_size,
	# 	body=body,
	# 	request_timeout=3600
	# 	)

	count = 0
	# outlist = []
	outDic = {}
	for doc in res:
		count+=1
		pageid = ckattr(doc['_source'], 'pageid')
		ads_keyword = ckattr(doc['_source'], 'ads_keyword')
		ads = ckattr(doc['_source'], 'ads')
		# print([pageid, ads_keyword, ads])
		if pageid in outDic:
			if ads_keyword in outDic[pageid]:
				if 'title' in outDic[pageid][ads_keyword] and 'descr' in outDic[pageid][ads_keyword]:
					if len(ads) > 0:
						for eachad in ads:
							if eachad['title'] in outDic[pageid][ads_keyword]['title']:
								continue
							else:	
								outDic[pageid][ads_keyword]['title'][eachad['title']] = 1
								outDic[pageid][ads_keyword]['descr'][eachad['descr']] = 1
					else:
						continue			
				else:
					outDic[pageid][ads_keyword]['title'] = {}
					outDic[pageid][ads_keyword]['descr'] = {}
			else:
				outDic[pageid][ads_keyword] = {}
		else:
			outDic[pageid] = {}
			outDic[pageid][ads_keyword] = {}
			outDic[pageid][ads_keyword]['title'] = {}
			outDic[pageid][ads_keyword]['descr'] = {}	
			if len(ads) > 0:
				for eachad in ads:
					if eachad['title'] in outDic[pageid][ads_keyword]['title']:
						continue
					else:	
						outDic[pageid][ads_keyword]['title'][eachad['title']] = 1
						outDic[pageid][ads_keyword]['descr'][eachad['descr']] = 1	
			else:
				continue							
			# outDic[pageid]['descr'] = {}
			# outDic[page]
		# if count > 10:
		# 	break

	logger.info('number of doc processed: '+str(count))			
	logger.info('Child proc done!')
	return outDic

if __name__ == '__main__':
	# pool = mp.Pool()
	with mp.Pool() as pool:
		res = pool.map(worker, range(numprocess))
	# print(str(len(res)))	
	# print(res)
	# for eachd in res:
	# 	print(eachd)


	finalDic = res[0]
	# print(str(len(res[1:])))
	# sys.exit()
	if len(res) > 1: # very ugly script
		for eachDic in res[1:]:
			for eachpageid in eachDic.keys():
				if eachpageid in finalDic:
					for eachkw in eachDic[eachpageid].keys():
						if eachkw in finalDic[eachpageid]:
							if 'title' in eachDic[eachpageid][eachkw] and 'title' in finalDic[eachpageid][eachkw]:
								for eachtitle in eachDic[eachpageid][eachkw]['title'].keys():
									if eachtitle in finalDic[eachpageid][eachkw]['title']:
										continue
									else:
										finalDic[eachpageid][eachkw]['title'][eachtitle] = 1
								for eachdescr in eachDic[eachpageid][eachkw]['descr'].keys():
									if eachdescr in finalDic[eachpageid][eachkw]['descr']:
										continue
									else:
										finalDic[eachpageid][eachkw]['descr'][eachdescr] = 1
							else:
								finalDic[eachpageid][eachkw]['title'] = {} 
								finalDic[eachpageid][eachkw]['descr'] = {}								
								continue			
						else:
							finalDic[eachpageid][eachkw] = {}
							finalDic[eachpageid][eachkw]['title'] = {} 
							finalDic[eachpageid][eachkw]['descr'] = {}
							if 'title' in eachDic[eachpageid][eachkw] and 'title' in finalDic[eachpageid][eachkw]:
								for eachtitle in eachDic[eachpageid][eachkw]['title'].keys():
									if eachtitle in finalDic[eachpageid][eachkw]['title']:
										continue
									else:
										finalDic[eachpageid][eachkw]['title'][eachtitle] = 1
								for eachdescr in eachDic[eachpageid][eachkw]['descr'].keys():
									if eachdescr in finalDic[eachpageid][eachkw]['descr']:
										continue
									else:
										finalDic[eachpageid][eachkw]['descr'][eachdescr] = 1
							else:
								finalDic[eachpageid][eachkw]['title'] = {} 
								finalDic[eachpageid][eachkw]['descr'] = {}								
								continue					
				else:
					finalDic[eachpageid] = {}
					for eachkw in eachDic[eachpageid].keys():
						if eachkw in finalDic[eachpageid]:
							if 'title' in eachDic[eachpageid][eachkw] and 'title' in finalDic[eachpageid][eachkw]:
								for eachtitle in eachDic[eachpageid][eachkw]['title'].keys():
									if eachtitle in finalDic[eachpageid][eachkw]['title']:
										continue
									else:
										finalDic[eachpageid][eachkw]['title'][eachtitle] = 1
								for eachdescr in eachDic[eachpageid][eachkw]['descr'].keys():
									if eachdescr in finalDic[eachpageid][eachkw]['descr']:
										continue
									else:
										finalDic[eachpageid][eachkw]['descr'][eachdescr] = 1
							else:
								finalDic[eachpageid][eachkw]['title'] = {} 
								finalDic[eachpageid][eachkw]['descr'] = {}								
								continue
						else:
							finalDic[eachpageid][eachkw] = {}
							finalDic[eachpageid][eachkw]['title'] = {} 
							finalDic[eachpageid][eachkw]['descr'] = {}
							if 'title' in eachDic[eachpageid][eachkw] and 'title' in finalDic[eachpageid][eachkw]:
								for eachtitle in eachDic[eachpageid][eachkw]['title'].keys():
									if eachtitle in finalDic[eachpageid][eachkw]['title']:
										continue
									else:
										finalDic[eachpageid][eachkw]['title'][eachtitle] = 1
								for eachdescr in eachDic[eachpageid][eachkw]['descr'].keys():
									if eachdescr in finalDic[eachpageid][eachkw]['descr']:
										continue
									else:
										finalDic[eachpageid][eachkw]['descr'][eachdescr] = 1
							else:
								finalDic[eachpageid][eachkw]['title'] = {} 
								finalDic[eachpageid][eachkw]['descr'] = {}								
								continue						

	# print(finalDic)
	logger.info('number of pageids stored in finalDic: '+str(len(finalDic)))

	# sys.exit()


	# mysql breaktime -h localhost -u root -p
	# Connect to the database
	conn = pymysql.connect(
			host='localhost',
			user='lance',
			password='',
			db='breaktime',
			charset='utf8mb4',
			cursorclass=pymysql.cursors.DictCursor
	)

	datalist = ['pageid', 'ads_keyword', 'ads_title', 'ads_descr', 'ads_num']
	insertcolstr = addacute(datalist)
	sqlparam = ','.join(['%s'] * len(datalist))

	# tablename = 'footprints_201802'
	count = 0

	droptbstr = '''DROP TABLE IF EXISTS '''+tablename
	createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
		(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
		pageid VARCHAR(40) NOT NULL,
		ads_keyword VARCHAR(100),
		ads_title TEXT,
		ads_descr TEXT,
		ads_num INT UNSIGNED,
		KEY `pageid` (`pageid`),
		KEY `ads_keyword` (`ads_keyword`),
		KEY `ads_num` (`ads_num`)
		) '''

	sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"
	logger.info(sql)
	each100000doc = []

	try:
		# logger.info('lance debug 01')
		with conn.cursor() as cursor:
			cursor.execute(droptbstr)
			cursor.execute(createtbstr)
			try:
				# while True: 
				for pageid in finalDic.keys():
					
					for keyword in finalDic[pageid].keys():
						count+=1
						if ckattr(finalDic[pageid][keyword], 'title') != '':
							titlestr = ':;:'.join(finalDic[pageid][keyword]['title'])
							descrstr = ':;:'.join(finalDic[pageid][keyword]['descr'])
							ads_num = len(finalDic[pageid][keyword]['title'])
						if ads_num > 0:
							each_doc = [pageid, keyword, titlestr, descrstr, ads_num]
							each100000doc.append(each_doc)
						else:
							continue
						if (count % 100000 == 0):
							try:
								cursor.executemany(sql, each100000doc)
								conn.commit()		
								each100000doc = [] 
							except Exception as err:
								logger.error('error while executemany(): '+str(err))				
							logger.info('number of doc processed: '+str(count))
							# if (count > 50000): # test 
							# 	break

			finally:	
				logger.info('length of last each100000doc: '+str(len(each100000doc)))
				try:
					cursor.executemany(sql, each100000doc)
					conn.commit()		
					each100000doc = [] # 
				except Exception as err:
					logger.error('error while executemany(): '+str(err))				
				logger.info('number of doc processed: '+str(count))					

	except Exception as err:
		logger.error('error while cursor(): '+str(err))

	finally:
		conn.close()	

	logger.info('to SQL done!')

