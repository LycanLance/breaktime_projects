#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
import pandas as pd
import numpy as np
import pymysql.cursors
import logging

# v1, add logging 
filename, ext = os.path.splitext(os.path.basename(__file__))

logger = logging.getLogger('insert_nlppages')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


# sys.exit()

'''=================== self-defined functions start ==================='''

# def log(msg):
#   print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	# outstr = outlist.join(',')
	outstr = ','.join(outlist)	
	# print(outstr)	
	return outstr

'''=================== self-defined functions end ==================='''

argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')

startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]

# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

body = {
  "_source": ["author_name","title","content_p","title_lv1","url"],
  "query": {
    "bool": {
      "must": [
        {"term" : { "hostname" : "zi.media"}
        }
      ]
    }
  }
}

res = helpers.scan(
				client = esCluster,
				scroll = '20m',
				size = 10000,
				query = body,
				index = 'nlppages',
				doc_type= "nlppage",
				request_timeout = 5000
)

# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='192.168.21.101',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)

datalist = ["author_name","title","content_p","title_lv1","url"]
insertcolstr = addacute(datalist)

tablename = 'nlppages'
count = 0

droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	author_name VARCHAR(100),
	title VARCHAR(300),
	content_p TEXT,
	title_lv1 VARCHAR(5),
	url VARCHAR(2083)
	) '''
# ENGINE=InnoDB DEFAULT CHARSET=utf8

each10000doc = []
try:
	with connection.cursor() as cursor:
		cursor.execute(droptbstr)
		cursor.execute(createtbstr)
		try:
			# while True: 
			for doc in res:
				# doc = res.next()
				# print(len(res))
				# break
				count+=1
				author_name = ckattr(doc['_source'], 'author_name')				 
				title = ckattr(doc['_source'], 'title')
				content_p = ckattr(doc['_source'], 'content_p')
				url = ckattr(doc['_source'], 'url')
				title_lv1 = ckattr(doc['_source'], 'title_lv1')
				each_doc = [author_name, title, content_p, title_lv1, url]
				each10000doc.append(each_doc)
				sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES (%s, %s, %s, %s, %s)"

				if (count % 10000 == 0):
					try:
						cursor.executemany(sql, each10000doc)
						# cursor.execute(sql, (each_doc))
						connection.commit()		
						each10000doc = [] # 
					except Exception as err:
						logger.error('error while executemany(): '+str(err))				
					logger.info('number of doc processed: '+str(count))
				# if (count > 50000): # test 
				# 	break

		finally:	
			logger.info('length of last each10000doc: '+str(len(each10000doc)))
			try:
				cursor.executemany(sql, each10000doc)
				connection.commit()		
				each10000doc = [] # 
			except Exception as err:
				logger.error('error while executemany(): '+str(err))				
			logger.info('number of doc processed: '+str(count))					

except Exception as err:
	logger.error('error while cursor(): '+str(err))

finally:
	connection.close()	

logger.info('Done!')