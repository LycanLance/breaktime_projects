#!/root/anaconda3/envs/my_env/bin/python
# from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
# import pandas as pd
# import numpy as np
import pymysql.cursors
import logging
# import json
# import re
# import hashlib # decided to use pagid instead 
# v1, from ads_2018March to another table. a narrow down process.
# sys.exit()

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


logger.info('Start!')

'''=================== self-defined functions start ==================='''


# def ckattr(po, attr):
# 	if attr in po.keys():
# 		# print(po[attr])
# 		return po[attr]
# 	else:
# 		if attr == 'stay':
# 			return 0
# 		else:	
# 			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	outstr = ','.join(outlist)	
	return outstr

'''=================== self-defined functions end ==================='''

# argvs = sys.argv[1:]
# indexStart = argvs.index('-S')
# indexEnd = argvs.index('-E')
# startDateString = argvs[indexStart+1]
# endDateString = argvs[indexEnd+1]
# indextb = argvs.index('-tb')
# tablename = argvs[indextb+1]
# indexesidx = argvs.index('-esidx')
# esidx = argvs[indexesidx+1]
# indexestype = argvs.index('-estype')
# estype = argvs[indexestype+1]


# ========== the code below should be a worker ==========


# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='192.168.21.101',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)

tb = 'ads_2018March'
try:
	droptbstr = '''DROP TABLE IF EXISTS '''+tb
	createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tb+'''
		(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		pageid VARCHAR(40) NOT NULL,
		ads_keyword VARCHAR(100),
		ads_title TEXT,
		ads_descr TEXT,
		ads_num INT UNSIGNED,
		KEY `pageid` (`pageid`),
		KEY `ads_keyword` (`ads_keyword`),
		KEY `ads_num` (`ads_num`)
		) '''
except NameError as e:
	logger.error(e)

# get data from ads_2018March
datalist = ['pageid', 'ads_keyword', 'ads_title', 'ads_descr']
colstr = addacute(datalist)
sqlparam = ','.join(['%s'] * len(datalist))

try:
	# mysql breaktime -h localhost -u root -p
	# Connect to the database
	conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
	count = 0
# first extract data from webpages
	with conn.cursor() as cursor:
		wherelist = []
		# wherelist.append("updated >= '20180301' + INTERVAL 8 HOUR")
		# wherelist.append("updated <= '20180331' + INTERVAL 8 HOUR")
		# if titlelv1_q != '':
		# 	wherelist.append("title_lv1 = '"+titlelv1_q+"'")
		if len(wherelist) > 0:
			whereclause = " where "+' AND '.join(wherelist)
		else: 
			whereclause = " "
		whereclause = whereclause + 'order by pageid limit 10'		
		sql = "select "+colstr+" from "+tb+whereclause
		# sql = "select "+colstr+" from "+tb+" limit 1000"
		logger.info(sql)		
		cursor.execute(sql)
		while True:
			rows = cursor.fetchmany(10000)
			if not rows:
				break
			for row in rows:
				count+=1
				pass
				print(row)
				if count > 50:
					sys.exit()
					break
				# toDict(row)
			# count+= 10000	
			if (count % 10000 == 0):
				logger.info('number of doc processed: '+str(count))
			
				
except Exception as err:        
	logger.error('debug_2: '+str(err))
finally:
	logger.info('number of doc processed: '+str(count))
	conn.close()

logger.info('Done!')	