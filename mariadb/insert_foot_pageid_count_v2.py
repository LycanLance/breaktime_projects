#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
import pandas as pd
import numpy as np
import pymysql.cursors
import logging
import multiprocessing as mp
# usage:
# nohup ./insert_foot_pageid_count.py -S 20180301 -E 20180331 -aggsize 10015017 -tb footprints_201803 -esidx footprints_201803 -estype footprint &
# v1, change lt to lte
# use search() instead of helper.scan()
# remember to set the aggs_size accordingly
# v2, multiprocessing. customize each process 


# read in params 
argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')
startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]
indextb = argvs.index('-tb')
tablename = argvs[indextb+1]

indexproc = argvs.index('-proc')
numprocess = int(argvs[indexproc+1])
indexesidx = argvs.index('-esidx')
esidx = argvs[indexesidx+1]
indexestype = argvs.index('-estype')
estype = argvs[indexestype+1]
indexaggsize = argvs.index('-aggsize')
aggs_size = argvs[indexaggsize+1]

# pid = os.getpid()
# print(pid)
# print([tablename, esidx, estype])
# sys.exit()
# prepare logger
filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
try:
	if tablename:
		fh = logging.FileHandler(filename+'_'+tablename+'.log')
except: 
	fh = logging.FileHandler(filename+'.log')	
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('Start!')
# sys.exit()




'''=================== self-defined functions start ==================='''

def ckattr(po, attr):
	if attr in po.keys():
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	outstr = ','.join(outlist)	
	return outstr

def toDateObj(indate):
	# from 20180301 to 2018-03-01
	return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))
	# return date[0:4]+'-'+date[4:6]+'-'+date[6:]

'''=================== self-defined functions end ==================='''

start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
# numprocess = 4 # must < delta.days
step = delta//numprocess

# for i in range(numprocess):
# 	worker_start = start + step*i
# 	if i == numprocess-1:
# 		worker_end = end
# 	else:
# 		worker_end = worker_start + step - timedelta(days=1)
# 	print([str(worker_start), str(worker_end)])

# print(tmp)
# print(tmp.days)
# print(type(tmp))
# sys.exit()

# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

def worker(stepnum):

	worker_start = start + step*stepnum
	if stepnum == numprocess-1:
		worker_end = end
	else:
		worker_end = worker_start + step - timedelta(days=1)

	body = {}
	# body['_source'] = ["pageid", "ads_keyword", "ads"]
	body['size'] = 0
	body['query'] = {}
	body['query']['bool'] = {}
	body['query']['bool']['must'] = []
	if '-zi' in sys.argv: # params for zi.media only
		body['query']['bool']['must'].append({"term": {"hostname": "zi.media"}})
	body['query']['bool']['must'].append({"range": { "updated": {"gte": str(worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})	
	# body['query']['bool']['must'].append({"range": { "updated": {"gte": startDateString, "lte": endDateString, "time_zone": "+08:00", "format": "yyyyMMdd"}}})
	body['aggs'] = {}
	body['aggs']['pageid_aggs'] = {}
	body['aggs']['pageid_aggs']['terms'] = {"field": "pageid", "size": aggs_size}

	# print(body)
	# sys.exit()
	logger.info(body)
	# sys.exit()
	res = esCluster.search(
		index=esidx,
		doc_type=estype,
		# scroll="3m",
		search_type="query_then_fetch",
		# size=scroll_size,
		body=body,
		request_timeout=3600
		)

	count = 0
	# outlist = []
	outDic = {}
	for doc in res['aggregations']['pageid_aggs']['buckets']:
		count+=1
		pageid = doc['key']
		footcount = doc['doc_count']
		outDic[pageid] = footcount
		# each_doc = [pageid, footcount]
		# outlist.append(each_doc)

	logger.info('number of doc processed: '+str(count))			
	logger.info('Done!')
	return outDic

if __name__ == '__main__':
	# pool = mp.Pool()
	with mp.Pool() as pool:
		res = pool.map(worker, range(numprocess))
		# print(str(len(res)))
		# print(res)

	finalDic = {}
	for eachDic in res:
		for eachKey in eachDic:
			# print(eachDic[eachKey])
			if eachKey in finalDic:
				finalDic[eachKey] += eachDic[eachKey]
			else:
				finalDic[eachKey] = eachDic[eachKey]

	# print(finalDic)
	logger.info('finalDic done!')
	logger.info('number of key in finalDic: '+str(len(finalDic)))
	# sys.exit()

	# mysql breaktime -h localhost -u root -p
	# Connect to the database
	conn = pymysql.connect(
			host='localhost',
			user='lance',
			password='',
			db='breaktime',
			charset='utf8mb4',
			cursorclass=pymysql.cursors.DictCursor
	)

	datalist = ['pageid', 'footcount']
	insertcolstr = addacute(datalist)
	sqlparam = ','.join(['%s'] * len(datalist))

	# tablename = 'footprints_201802'
	count = 0

	# afe7662a9dfff8a6a07a06fba67f240e382bb23a
	# 1ab0f842eee40ffbf6e730da282976ce1522540794
	# f90b6ffa4139b64fcb1461123abd288090d8b324
	# 1ab0f842eee40ffbf6e730da282976ce
	droptbstr = '''DROP TABLE IF EXISTS '''+tablename
	createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
		(pageid VARCHAR(40) NOT NULL PRIMARY KEY,
		footcount INT UNSIGNED NOT NULL,
		KEY `footcount` (`footcount`)
		) '''
	# id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	# url VARCHAR(2083)	
	# ENGINE=InnoDB DEFAULT CHARSET=utf8

	sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"
	logger.info(sql)
	each100000doc = []

	try:
		# logger.info('lance debug 01')
		with conn.cursor() as cursor:
			cursor.execute(droptbstr)
			cursor.execute(createtbstr)
			try:
				# while True: 
				for key in finalDic.keys():
					count+=1
					pageid = key
					footcount = finalDic[key]
					each_doc = [pageid, footcount]
					# print(each_doc)
					# continue
					each100000doc.append(each_doc)
					# sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"
					if (count % 100000 == 0):
						try:
							cursor.executemany(sql, each100000doc)
							# cursor.execute(sql, (each_doc))
							conn.commit()		
							each100000doc = [] # 
						except Exception as err:
							logger.error('error while executemany(): '+str(err))				
						logger.info('number of doc processed: '+str(count))
					# if (count > 50000): # test 
					# 	break

			finally:	
				logger.info('length of last each100000doc: '+str(len(each100000doc)))
				try:
					cursor.executemany(sql, each100000doc)
					conn.commit()		
					each100000doc = [] # 
				except Exception as err:
					logger.error('error while executemany(): '+str(err))				
				logger.info('number of doc processed: '+str(count))					

	except Exception as err:
		logger.error('error while cursor(): '+str(err))

	finally:
		conn.close()	

	logger.info('to SQL done!')

