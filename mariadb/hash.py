import hashlib
mystring = input('Enter String to hash: ')
# print(b'')
# Assumes the default UTF-8
hash_object = hashlib.md5(mystring.encode())
hex_dig = hash_object.hexdigest()
print(hex_dig)
byte_dig = hash_object.digest()
print(byte_dig)

# hash_object = hashlib.sha1(b'Hello World')
# hash_object = hashlib.sha1(mystring.encode())
# hex_dig = hash_object.hexdigest()
# print(hex_dig)


