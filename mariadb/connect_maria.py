#!/root/anaconda3/envs/my_env/bin/python

# import mysql.connector as mariadb


# mariadb_connection = mariadb.connect(user='root', password='', database='mysql')
# cursor = mariadb_connection.cursor()


# # cursor.execute("SELECT first_name,last_name FROM employees WHERE first_name=%s", (some_name,))

# cursor.execute("SELECT user,host FROM user WHERE user='host'")
# # sys.exit()

# for user,host in cursor:
#     print("user: {}, host: {}").format(user,host)



import pymysql.cursors
import sys

# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Create a new record
        sql = "INSERT INTO `ads` (`keyword`, `ads`) VALUES (%s, %s)"
        cursor.execute(sql, ('Naruto', 'Daggers'))

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

    with connection.cursor() as cursor:
        # Read a single record
        sql = "SELECT `keyword`, `ads` FROM `ads` WHERE `keyword`=%s"
        cursor.execute(sql, ('Naruto',))
        result = cursor.fetchone()
        print(result)
finally:
    connection.close()