#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
import pandas as pd
import numpy as np
import pymysql.cursors
import logging

# usage:
# nohup ./insert_foot_pageid_count.py -S 20180301 -E 20180331 -aggsize 10015017 -tb footprints_201803 -esidx footprints_201803 -estype footprint &
# v1, change lt to lte
# use search() instead of helper.scan()
# remember to set the aggs_size accordingly

# read in params 
argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')
startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]
indextb = argvs.index('-tb')
tablename = argvs[indextb+1]
indexesidx = argvs.index('-esidx')
esidx = argvs[indexesidx+1]
indexestype = argvs.index('-estype')
estype = argvs[indexestype+1]
indexaggsize = argvs.index('-aggsize')
aggs_size = argvs[indexaggsize+1]

pid = os.getpid()
# print(pid)
# print([tablename, esidx, estype])
# sys.exit()
# prepare logger
filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'_'+tablename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('Start!')
# sys.exit()
'''=================== self-defined functions start ==================='''

# def log(msg):
#   print('['+str(datetime.datetime.now().isoformat(timespec='seconds'))+']'+msg)

def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	# outstr = outlist.join(',')
	outstr = ','.join(outlist)	
	# print(outstr)	
	return outstr

'''=================== self-defined functions end ==================='''



# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

# aggs_size = 10015017
# body = {
#   # "_source": ["url", "title", "pageid", "fp", "hostname", "os_name", "name", "stay", "updated"],
#   "size": 0,
#   "query": {
#     "bool": {
#       "must": [
#         # {"term" : { "hostname" : "zi.media"}
#         # },
#         {
#           "range": {
#             "updated": {
#               "gte": startDateString,
#               "lte": endDateString,
#               "time_zone": "+08:00",
#               "format": "yyyyMMdd"
#             }
#           }
#         }
#       ]
#     }
#   },
#   "aggs": {
#     "pageid_aggs": {
#       "terms": {
#         "field": "pageid",
#         "size": aggs_size,
#         # "order": {
#         #   "_count": "desc"
#         # }
#       }
#     }
#   }
# }

body = {}
# body['_source'] = ["pageid", "ads_keyword", "ads"]
body['size'] = 0
body['query'] = {}
body['query']['bool'] = {}
body['query']['bool']['must'] = []
if '-zi' in sys.argv: # params for zi.media only
	body['query']['bool']['must'].append({"term": {"hostname": "zi.media"}})
body['query']['bool']['must'].append({"range": { "updated": {"gte": startDateString, "lte": endDateString, "time_zone": "+08:00", "format": "yyyyMMdd"}}})
body['aggs'] = {}
body['aggs']['pageid_aggs'] = {}
body['aggs']['pageid_aggs']['terms'] = {"field": "pageid", "size": aggs_size}

# print(body)
# sys.exit()


# "aggs": {
# 	"3": {
# 		"cardinality": {
# 			"field": "fp"
# 		}
# 	}
# }
# https://stackoverflow.com/questions/22927098/show-all-elasticsearch-aggregation-results-buckets-and-not-just-10
# It is recommended to explicitly set reasonable value for size a number between 1 to 2147483647.

logger.info(body)
# sys.exit()
res = esCluster.search(
	index=esidx,
	doc_type=estype,
	# scroll="3m",
	search_type="query_then_fetch",
	# size=scroll_size,
	body=body,
	request_timeout=3600
	)


# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='localhost',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)

datalist = ['pageid', 'footcount']
insertcolstr = addacute(datalist)
sqlparam = ','.join(['%s'] * len(datalist))

# tablename = 'footprints_201802'
count = 0

# afe7662a9dfff8a6a07a06fba67f240e382bb23a
# 1ab0f842eee40ffbf6e730da282976ce1522540794
# f90b6ffa4139b64fcb1461123abd288090d8b324
# 1ab0f842eee40ffbf6e730da282976ce
droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(pageid VARCHAR(40) NOT NULL PRIMARY KEY,
	footcount INT UNSIGNED NOT NULL,
	KEY `footcount` (`footcount`)
	) '''
# id INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
# url VARCHAR(2083)	
# ENGINE=InnoDB DEFAULT CHARSET=utf8

sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"
logger.info(sql)
each10000doc = []
try:
	# logger.info('lance debug 01')
	with connection.cursor() as cursor:
		cursor.execute(droptbstr)
		cursor.execute(createtbstr)
		try:
			# while True: 
			for doc in res['aggregations']['pageid_aggs']['buckets']:
				count+=1
				pageid = doc['key']
				footcount = doc['doc_count']
				each_doc = [pageid, footcount]
				# print(each_doc)
				# continue
				each10000doc.append(each_doc)
				# sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"
				if (count % 10000 == 0):
					try:
						cursor.executemany(sql, each10000doc)
						# cursor.execute(sql, (each_doc))
						connection.commit()		
						each10000doc = [] # 
					except Exception as err:
						logger.error('error while executemany(): '+str(err))				
					logger.info('number of doc processed: '+str(count))
				# if (count > 50000): # test 
				# 	break

		finally:	
			logger.info('length of last each10000doc: '+str(len(each10000doc)))
			try:
				cursor.executemany(sql, each10000doc)
				connection.commit()		
				each10000doc = [] # 
			except Exception as err:
				logger.error('error while executemany(): '+str(err))				
			logger.info('number of doc processed: '+str(count))					

except Exception as err:
	logger.error('error while cursor(): '+str(err))

finally:
	connection.close()	

logger.info('Done!')