#!/root/anaconda3/envs/my_env/bin/python
import pymysql.cursors
import sys
import logging
import os.path
import pandas as pd
import numpy as np
import csv
logger = logging.getLogger('select_tc')
logger.setLevel(logging.DEBUG)
filename, ext = os.path.splitext(os.path.basename(__file__))
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

# Connect to the database
conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

conplist = []
try:
# second, extract data from footprints_201803

	count = 0
	with conn.cursor() as cursor:
		# sql = "select pageid,count(id) as count from footprints_201803 group by pageid order by count desc limit 5"
		sql = "select title_lv1,title,url from nlppages"		
		cursor.execute(sql)
		while True:
			rows = cursor.fetchmany(10000)
			count+= 10000
			logger.info('processed nlppages count: '+str(count))
			if not rows:
				break
			for row in rows:
				pass
				conplist.append([row['title_lv1'], row['title'], row['url']])
				# print(row)	
except Exception as err:        
	logger.error(err)
finally:
    conn.close()
    # pageidDict.clear()

logger.info(str(len(conplist)))
# logger.info(np.array(conplist).shape)
csvfile = 'select_tc.csv'
with open(csvfile, "w") as output:
    writer = csv.writer(output, lineterminator='\n')
    for val in conplist:
        writer.writerow(val)


# df = pd.DataFrame({'content_p': conplist})
# df = pd.DataFrame(np.array(conplist))
# df.to_csv('select_tc.csv', index=False)    
logger.info('Done!')