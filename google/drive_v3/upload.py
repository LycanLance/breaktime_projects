"""
Shows basic usage of the Drive v3 API.

Creates a Drive v3 API service and prints the names and ids of the last 10 files
the user has access to.
"""
from __future__ import print_function
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools
from googleapiclient.http import MediaFileUpload
import sys
import pprint
import os
# Setup the Drive v3 API
SCOPES = [
    # 'https://www.googleapis.com/auth/drive.readonly',
    'https://www.googleapis.com/auth/drive'
]
store = file.Storage('credentials.json')
creds = store.get()
if not creds or creds.invalid:
    flow = client.flow_from_clientsecrets('client_secret.json', SCOPES)
    creds = tools.run_flow(flow, store)
service = build('drive', 'v3', http=creds.authorize(Http()))


def createfolder(name, folderid):

    file_metadata = {
        'name': name,
        'mimeType': 'application/vnd.google-apps.folder',
        'parents': [folderid]
    }
    file = service.files().create(
        body=file_metadata,
        supportsTeamDrives=True,
        fields='id').execute()

    print('Folder ID: %s' % file.get('id'))
    return file.get('id')


def uploadfile(filepath, folderid):
    # print(filepath, folderid)
    # sys.exit()

    tmpl = filepath.split('/')
    filename = tmpl[-1]
    folder_id = folderid
    file_metadata = {
        'name': filename,
        'parents': [folder_id]
    }

    # mime_type = 'application/vnd.google-apps.unknown'
    # 5MB = 1024*1024*5 B
    if os.stat(filepath).st_size <= 1024 * 1024 * 5:
        print('file size smaller than 5MB')
        media = MediaFileUpload(filepath,
                                # mimetype=mime_type,
                                resumable=True)
    else:
        print('file size larger than 5MB')
        media = MediaFileUpload(filepath,
                                # mimetype=mime_type,
                                chunksize=1024 * 1024 * 4,
                                resumable=True)

    # print(media.__dict__)
    # print(media.__doc__)
    # print(dir(media))
    # sys.exit()

    # media = MediaFileUpload(filepath,
    #                         # mimetype='image/jpeg',
    #                         resumable=True)
    file = service.files().create(body=file_metadata,
                                  media_body=media,
                                  supportsTeamDrives=True,
                                  fields='id').execute()
    print('File ID: %s' % file.get('id'))

# sys.exit()


def listfile():
    pageToken = ''
    count = 0
    while True:
        count += 1
        print(str(count))
        results = service.files().list(
            pageSize=1,
            pageToken=pageToken,
            corpora='teamDrive',
            includeTeamDriveItems=True,
            supportsTeamDrives=True,
            # supportsTeamDriveItems=True,
            # includeTeamDrives=True,
            teamDriveId="0ABN1nlDroMTQUk9PVA",
            # useDomainAdminAccess=True,
            # q="name='Category'",
            # q="mimeType = 'application/vnd.google-apps.folder'"
            # q="'1gqOUqb2fxNuhieS3QxFp0T6Uk_BOuNhy' in parents", # Training Dataset dir
            q="'13M1ItMZjdqIAzHXLNimYRE7nPoq8xjeo' in parents ",  # yahoo 商品資料 dir

            # fields="files(id,teamDriveId,name,mimeType,capabilities/canShare)",
            fields="*"
        ).execute()
        # if results['nextPageToken']:
        pprint.pprint(results)
        items = results.get('files', [])
        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print('{0} ({1})'.format(item['name'], item['id']))

        try:
            pageToken = results['nextPageToken']
        except KeyError:
            break
        # print(pageToken)

    # and mimeType = 'application/vnd.google-apps.folder'
    # name == 'Yahoo 商品資料'
    # or maybe "files(id,name,capabilities/canShare),nextPageToken"

    # pageSize=10,
    # fields="nextPageToken, files(id, name)"


def checkfile(name, folderid, typename, teamDriveId=None):
    # either file or folder
    pass
    pageToken = None
    qlist = []
    qlist.append('\'' + folderid + '\' in parents')
    qlist.append('name = \'' + name + '\'')
    qstr = ' and '.join(qlist)
    try:
        results = service.files().list(
            pageSize=10,
            pageToken=pageToken,
            corpora='teamDrive',
            includeTeamDriveItems=True,
            supportsTeamDrives=True,
            teamDriveId=teamDriveId,
            q=qstr,  # yahoo 商品資料 dir
            fields="files(id)",
            # fields="files(id,teamDriveId,name,mimeType,capabilities/canShare)",
            # fields="*"
        ).execute()
    except:
        return None
    # pprint.pprint(results)
    items = results.get('files', [])
    # print(str(len(items)))
    if len(items) == 0:
        print(typename + ' ' + name + ' does not exists!')
        return 0
    elif len(items) > 1:
        print('should not have ' + typename + ' with the same name!')
        return 1
    else:
        return items[0]['id']


if __name__ == '__main__':
    # createfolder(name='test', folderid='1IwSSD5t6nwg9K058UJl7IlN5Vhvdt_Ro')

    # step 1: check if the target folder exists. If not, create one.
    FOLDERID = '13M1ItMZjdqIAzHXLNimYRE7nPoq8xjeo'
    TEAMDRIVEID = '0ABN1nlDroMTQUk9PVA'

    targetfoldername = 'test2'
    targetdirid = checkfile(name=targetfoldername, folderid=FOLDERID,
                            teamDriveId=TEAMDRIVEID, typename='folder')
    if targetdirid == 0:  # if not exist create one
        pass
        targetdirid = createfolder(
            name=targetfoldername, folderid=FOLDERID)
        print('folder does not exist, creat new folder with id = ' + targetdirid)
    elif targetdirid == 1:
        print('duplicated folder name: ' + targetfoldername + '. exit!')
        sys.exit()
        pass
    else:
        # dir exists, no need to create another one
        print('dir exists, no need to create another one')
        pass

# step 2: check if the target file exists. If yes skip uploading, else upload.

    filepath = '/home/lance/projects/database/yahooEC/bid/full/2018051506/bid_000000.json.gzip'
    tmpl = filepath.split('/')
    filename = tmpl[-1]

    fileid = checkfile(name=filename, folderid=targetdirid,
                       teamDriveId=TEAMDRIVEID, typename='file')
    if fileid == 0:
        uploadfile(filepath=filepath,
                   folderid=targetdirid)
    elif fileid == 1:
        print('duplicated file name: ' + filename + '. exit!')
        sys.exit()
        pass
        # print('File already exist id: ' + fileid)
    else:
        print('file exists, no need to upload another one')
        pass
        # file exists, no need to upload another one
