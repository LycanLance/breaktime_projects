#!/root/anaconda3/envs/my_env/bin/python
import pandas as pd
import sys
import datetime
import matplotlib
# Force matplotlib to not use any Xwindows backend.
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# print(
#     datetime.datetime.fromtimestamp(int("1284101485")).strftime('%Y-%m-%d %H:%M:%S')
# )

itype = sys.argv[1]
conf = sys.argv[2]

labellist = []
listofdf = []
for aline in open(conf, 'r'):
	line = aline.strip()
	if not line.startswith("#") and not (line==""):
		param = line.split(",")
		label = param[0]
		infile = param[1]
		labellist.append(label)
		listofdf.append(pd.read_csv(infile))


outputfile_count = itype+'_dataloss_count.csv'
outputfile_mavg = itype+'_dataloss_mavg.csv'

timedf = pd.DataFrame()
# timeindex = [] # should be data.series
countdf = pd.DataFrame()
mavgdf = pd.DataFrame()

listofdf2 = []
count = 0
for eachdf in listofdf:
	outdf = pd.DataFrame()
	eachdf.columns=['time', 'count', 'mavg']
	outdf = eachdf.iloc[1:216, 0:]
	if count == 0:
		for i in range(len(outdf)):
			# i should use 0409 as time label
			outdf.iloc[i, 0] = datetime.datetime.fromtimestamp(int(outdf.iloc[i, 0])/1000).strftime('%Y-%m-%d %H:%M:%S')
			# outdf.iloc[i, 0] = datetime.datetime.fromtimestamp(int(outdf.iloc[i, 0])/1000).strftime('%H:%M:%S')
		timeds = outdf.iloc[:, 0] 	
	listofdf2.append(outdf)
	countds = outdf['count']
	countdf = pd.concat([countdf, countds], axis=1)
	mavgds = outdf['mavg']
	mavgdf = pd.concat([mavgdf, mavgds], axis=1)	
	count+=1

##
countdf.index = timeds
countdf.columns = labellist
mavgdf.index = timeds
mavgdf.columns = labellist
mavgdf[labellist] = mavgdf[labellist].astype(float)

## 
for eachdf in [countdf, mavgdf]:
	count=0
	for i in range(len(eachdf)):
		row = eachdf.index.tolist()[i]
		eachdf.loc[row,'sum'] = eachdf.loc[row,labellist].sum()
		eachdf.loc[row,'mean'] = eachdf.loc[row,labellist].mean()
		eachdf.loc[row,'std'] = eachdf.loc[row,labellist].std()
		count+=1
	eachdf = eachdf.round(2)	
	eachdf.reset_index(inplace=True)
	# print(eachdf.head())

# print(countdf.dtypes)
# plt.plot(countdf.index, countdf[labellist])
# countdf.plot()
# plt.savefig('tmp_001')
countdf.to_csv(outputfile_count, float_format='%.2f')
mavgdf.to_csv(outputfile_mavg, float_format='%.2f')

# plt.scatter()
sys.exit()	

