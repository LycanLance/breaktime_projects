'''
v1,
calculate the top 10 keywords with the most ads exposed for queried path
inherit from query_ads_by_path.py
calculate the ads stat from Saturday 8:00 AM to Sunday 10:00 PM
v2, given a time range, calculate the daily ads stat.
hostname and filterout params are not yet parameterized
'''
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta, datetime
import csv
import sys
import os
import pandas as pd
# import numpy as np
# import pymysql.cursors
import logging
import multiprocessing as mp
import pandas as pd
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)


params = ['S', 'E', 'out', 'top']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

# esidx = paramDic['esidx']
# estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
# qpath = paramDic['path']
label = paramDic['out']
top = int(paramDic['top'])
# outfile = paramDic['out']

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    if attr in po.keys():
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    # print(indate)
    return datetime(int(indate[0:4]), int(indate[4:6]), int(indate[6:8]), hour=int(indate[8:10]), minute=int(indate[10:12]), second=int(indate[12:14]))
    # return date[0:4]+'-'+date[4:6]+'-'+date[6:]


'''=================== self-defined functions end ==================='''

filterout = ['/wifi', '/adwifi', '/tymetroWiFI']
# arbitrarily defined
startObj = datetime.strptime(startDateString, '%Y%m%d')
endObj = datetime.strptime(endDateString, '%Y%m%d')
start = startObj.strftime('%Y-%m-%d')
end = endObj.strftime('%Y-%m-%d')

q_date = []
tmpObj = startObj
while (tmpObj <= endObj):
    q_date.append(tmpObj.strftime('%Y-%m-%d'))
    tmpObj = tmpObj + timedelta(days=1)

q_range = [i for i in range(len(q_date))]
dateDict = dict(zip(q_range, q_date))
q_list = q_range

proc = 8
stepsize = len(
    q_list) // proc if len(q_list) % proc < proc // 2 else len(q_list) // proc + 1  # 4

# connection to ELK
esCluster = Elasticsearch(
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000
)


def worker(stepnum):
    pass
    # there are a total of 38 hours of interest
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    returnlist = []
    for i in todoworklist:
        body = {}
        body['_source'] = ["ads_keyword", "ads"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        # body['query']['bool']['filter'].append({"term": {"hostname": qpath}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": dateDict[i], "lte": dateDict[i], "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        # for wifi in filterout:
        #     body['query']['bool']['must_not'].append(
        #         {"prefix": {"path": wifi}})
        # ref: https://www.elastic.co/guide/en/watcher/current/trigger.html#schedule-daily
        logger.info(body)

        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='ads',
            doc_type='ad',
            request_timeout=5000
        )

        Dict = {}
        try:
            count = 0
            zerocount = 0
            for doc in res:
                kw = ckattr(doc['_source'], 'ads_keyword')
                adlist = ckattr(doc['_source'], 'ads')
                adcount = 0
                if adlist != '':
                    adcount = len(adlist)
                if kw in Dict:
                    Dict[kw]['sum'] += adcount
                    Dict[kw]['count'] += 1
                    if adcount == 0:
                        zerocount += 1
                        Dict[kw]['zero'] += 1
                else:
                    Dict[kw] = {}
                    Dict[kw]['sum'] = adcount
                    Dict[kw]['count'] = 1
                    Dict[kw]['zero'] = 0
                count += 1
            logger.info('total number of zero count: ' + str(zerocount))
            logger.info('total number of doc processed: ' + str(count))
            logger.info('total number of kw processed: ' + str(len(Dict)))
        except Exception as e:
            logger.error(e)

        xtmp = {}
        for xkw in Dict:
            Dict[xkw]['avg'] = Dict[xkw]['sum'] / Dict[xkw]['count']
            xtmp[xkw] = Dict[xkw]['avg']
        sortedk = sorted(xtmp, key=xtmp.get, reverse=True)

        # try:
        outfile = 'data/' + label + '_' + str('%02d' % (i + 1)) + '.csv'
        # except:
        #     print(label, i)

        # with open(outfile, 'w') as f:
        #     writer = csv.writer(f)
        #     writer.writerow([workerstart, wokerend])
        #     writer.writerow(['keword', 'sum', 'count', 'avg'])
        #     for kw in sortedk:
        #         writer.writerow([kw, Dict[kw]['sum'], Dict[kw]
        #                          ['count'], Dict[kw]['avg']])

        returnlist.append(Dict)

        tolist = []
        for ykw in Dict:
            tolist.append(
                {'keyword': ykw, 'adscount': Dict[ykw]['sum'], 'zerocount': Dict[ykw]['zero'], 'kwcount': Dict[ykw]['count'], 'avg': Dict[ykw]['avg']})
        df = pd.DataFrame(tolist)
        df = df[['keyword', 'adscount', 'zerocount', 'kwcount', 'avg']]
        df = df.sort_values(['avg', 'kwcount'], ascending=[False, False])
        df = df.iloc[:top]
        df.to_csv(outfile, index=False)

    return returnlist
    # if count < 10:
    #     print(i + ',' + str(Dict[i]))
    # count += 1


def multiproc():
    pass
    global q_list
    procc = proc if proc <= len(q_list) else len(q_list)
    with mp.Pool() as pool:
        res = pool.map(worker, range(procc))

    # sys.exit()
    joinDict = {}
    # tmp = {}
    # tmp['sum'] = 0
    # tmp['count'] = 0
    # tmp['avg'] = 0
    for ilist in res:
        for xdic in ilist:
            for k in xdic.keys():
                if k in joinDict:
                    # pass
                    joinDict[k]['sum'] += xdic[k]['sum']
                    joinDict[k]['count'] += xdic[k]['count']
                    joinDict[k]['zero'] += xdic[k]['zero']
                else:
                    joinDict[k] = {}
                    joinDict[k]['sum'] = xdic[k]['sum']
                    joinDict[k]['count'] = xdic[k]['count']
                    joinDict[k]['zero'] = xdic[k]['zero']
                    # pass

    tmp = {}
    for k in joinDict:
        # tmp = {}
        # for kw in Dict:
        joinDict[k]['avg'] = joinDict[k]['sum'] / joinDict[k]['count']
        tmp[k] = joinDict[k]['avg']
    sortedk = sorted(tmp, key=tmp.get, reverse=True)

    outfile = 'data/' + label + '_sum.csv'

    # with open(outfile, 'w') as f:
    #     writer = csv.writer(f)
    #     writer.writerow(['keword', 'sum', 'count', 'avg'])
    #     for kw in sortedk:
    #         writer.writerow(
    #             [kw, joinDict[kw]['sum'], joinDict[kw]['count'], joinDict[kw]['avg']])

    tolist = []
    # decide what header will be shown in the csv!
    for ykw in joinDict:
        tolist.append(
            {'keyword': ykw, 'adscount': joinDict[ykw]['sum'], 'zerocount': joinDict[ykw]['zero'], 'kwcount': joinDict[ykw]['count'], 'avg': joinDict[ykw]['avg']})
    df = pd.DataFrame(tolist)
    df = df[['keyword', 'adscount', 'zerocount', 'kwcount', 'avg']]
    df = df.sort_values(['avg', 'kwcount'], ascending=[False, False])
    df.to_csv(outfile, index=False)


if __name__ == '__main__':
    multiproc()
    logger.info('Done!')
