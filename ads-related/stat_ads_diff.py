'''
summarize data by hour
support Simpson's Diversity Index calculation
'''

import sys
import logging
import glob
import os
import re
import pandas as pd
import csv
# params = ['a', 'b']
# # q_list = []
# argvs = sys.argv
# # print(argvs)
# paramDic = {}
# for param in params:
#     if '-' + param in argvs:
#         idx = argvs.index('-' + param)
#         paramDic[param] = argvs[idx + 1]
#     else:
#         err = 1
#         print('param -' + param + ' is missing!')
# try:
#     if err:
#         sys.exit(1)
# except Exception as e:
#     pass

# a = paramDic['a']
# b = paramDic['b']

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)
logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


labels = ['range0407_08', 'range0414_15', 'range0421_22', 'range0519_20']

a = labels[0]  # get the first element only
# print(a)
# sys.exit()
# afilelist = glob.glob(a + '*.csv')
afilelist = glob.glob('data/' + a + '*.csv')
# print(afilelist)
# print(str(len(afilelist)))

tags = []
for i in afilelist:
    mObj = re.match('data/' + a + '_(\S+?)\.csv', i)
    # print(mObj.group(1))
    if mObj.group(1) != 'sum':
        tags.append(mObj.group(1))

tags.sort()
print(tags)

# sys.exit()
labelDict = {}
for label in labels:
    summarylist = []
    for hour in tags:  # hours
        infile = 'data/' + label + '_' + hour + '.csv'
        with open(infile) as csvfile:
            csvreader = csv.reader(csvfile)
            count = 0
            adscountsum = 0
            zerocountsum = 0
            kwcountsum = 0
            zerocountsum = 0
            fillratesum = 0
            numeratorsum = 0
            for row in csvreader:
                if row[0] == 'keyword':  # skip first row
                    continue
                count += 1
                kw = row[0]
                # try:
                adscount = int(row[1])
                zerocount = int(row[2])
                kwcount = int(row[3])
                fillrate = 1 - (zerocount / kwcount)
                # except:
                # print(row)
                adscountsum += adscount
                kwcountsum += kwcount
                fillratesum += fillrate
                numeratorsum += kwcount * (kwcount - 1)
                if adscount == 0:
                    zerocountsum += 0
            # Simpson's Diversity
            diversity = 1 - (numeratorsum / (kwcountsum * (kwcountsum - 1)))
            # print(diversity, numeratorsum, count)
            avg = adscountsum / kwcountsum
            avgfillrate = fillratesum / count
            # fillrate = 1 - (zerocountsum / count)
            # uqkecount, avg, fillrate
            summary = (count, avg, avgfillrate,
                       adscountsum, kwcountsum, diversity)
            summarylist.append(summary)
            # print(summary)
    # print(summarylist)
    labelDict[label] = summarylist


# for k, v in labelDict.items():
#     for iv in v:
#         # a, b, c = iv
#         print(k, iv[0], iv[1], iv[2])
# sys.exit()
# print(row)
# df = pd.read_csv(infile)
# print(df)
# bfile = 'data/' + b + '_' + i + '.csv'
# print((afile, bfile))
# break

# filetypes = ['uqkwcount', 'avg', 'fillrate']
filemapping = {
    0: 'uqkwcount',
    1: 'avg',
    2: 'fillrate',
    3: 'adscount',
    4: 'kwcount',
    5: 'diversity'
}

for k, v in filemapping.items():
    tolist = []
    # tmpDict = {}
    for i in range(len(tags)):  # prepare each index
        tmpDict = {}
        for label in labels:
            # tmpDict[label] = 0
            tmpDict[label] = labelDict[label][i][k]
            # print(ituple)
            # tmpDict[label].app(ituple[k])
            # tmpDict[label] = ituple[k]
        tolist.append(tmpDict)
    # print(tolist)
    df = pd.DataFrame(tolist)
    # print(df)
    outfile = 'info/' + v + '.csv'
    df.to_csv(outfile, index=True)

sys.exit()

# tolist = []
# for label in labelDict:
#     tolist.append(
#         {'keyword': ykw, 'sum': joinDict[ykw]['sum'], 'zero': joinDict[ykw]['zero'], 'count': joinDict[ykw]['count'], 'avg': joinDict[ykw]['avg']})
# df = pd.DataFrame(tolist)
# df = df[['keyword', 'sum', 'zero', 'count', 'avg']]


# stop here
