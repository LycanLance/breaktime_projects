#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
import pandas as pd
import numpy as np
import pymysql.cursors
import logging

# usage:
# nohup ./insert_nlpkeyword.py -S 20180131 -E 20180401 &
# v1, time range: 20180131 to 20180417
# adjust time range to 20180131 to 20180401
# nlp api hasn't work on those data after 20180401

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	# outstr = outlist.join(',')
	outstr = ','.join(outlist)	
	# print(outstr)	
	return outstr

'''=================== self-defined functions end ==================='''

argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')
startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]
indextb = argvs.index('-tb')
tablename = argvs[indextb+1]

# indexproc = argvs.index('-proc')
# numprocess = int(argvs[indexproc+1])
indexesidx = argvs.index('-esidx')
esidx = argvs[indexesidx+1]
indexestype = argvs.index('-estype')
estype = argvs[indexestype+1]
# indexaggsize = argvs.index('-aggsize')
# aggs_size = argvs[indexaggsize+1]


# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

body = {
  "_source": ["content_h1_keyword", "content_h2_keyword", "title_lv1", "title_lv2", "title_lv3", "title_keyword", "content_keyword", "content_p_keyword", "updated", "url", "hostname"],
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "updated": {
              "gte": startDateString,
              "lte": endDateString,
              "time_zone": "+08:00",
              "format": "yyyyMMdd"
            }
          }
        }
      ]
    }
  }
}
# print(body)
res = helpers.scan(
				client = esCluster,
				scroll = '20m',
				size = 10000,
				query = body,
				index = esidx,
				doc_type= estype,
				request_timeout = 5000
)

# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='192.168.21.101',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)
# ["content_h1_keyword", "content_h2_keyword", "title_lv1", "title_lv2", "title_lv3", "title_keyword", "content_keyword", "content_p_keyword", "updated", "url"]
datalist = ['nlpid', 'hostname', 'title_lv1', 'title_lv2', 'title_lv3', 'title_k', 'con_h1_k', 'con_h2_k', 'con_p_k', 'con_k', 'url', 'updated']
insertcolstr = addacute(datalist)
sqlparam = ','.join(['%s'] * len(datalist))
# print(sqlparam)
# sys.exit()

# tablename = 'nlppages_20180131_20180417'
count = 0

droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nlpid VARCHAR(100),
	hostname VARCHAR(100),
	title_lv1 VARCHAR(100),
	title_lv2 VARCHAR(100),
	title_lv3 VARCHAR(100),
	title_k VARCHAR(100),
	con_h1_k VARCHAR(100),
	con_h2_k VARCHAR(100),
	con_p_k VARCHAR(100),
	con_k VARCHAR(100),
	url TEXT,
	updated DATETIME,
	KEY `nlpid` (`nlpid`),
	KEY `hostname` (`hostname`),
	KEY `updated` (`updated`) 
	) '''

each10000doc = []
try:
	with connection.cursor() as cursor:
		cursor.execute(droptbstr)
		cursor.execute(createtbstr)
		try:
			# while True: 
			for doc in res:
				count+=1
				nlpid = doc['_id']
				hostname = ckattr(doc['_source'], 'hostname')
				title_lv1 = ckattr(doc['_source'], 'title_lv1')
				title_lv2 = ckattr(doc['_source'], 'title_lv2')
				title_lv3 = ckattr(doc['_source'], 'title_lv3')
				title_k = ckattr(doc['_source'], 'title_keyword')
				con_h1_k = ckattr(doc['_source'], 'content_h1_keyword')
				con_h2_k = ckattr(doc['_source'], 'content_h2_keyword')
				con_p_k = ckattr(doc['_source'], 'content_p_keyword')
				con_k = ckattr(doc['_source'], 'content_keyword')
				url = ckattr(doc['_source'], 'url')
				updated = ckattr(doc['_source'], 'updated')
				each_doc = [nlpid, hostname, title_lv1, title_lv2, title_lv3, title_k, con_h1_k, con_h2_k, con_p_k, con_k, url, updated]
				each10000doc.append(each_doc)
				sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"

				if (count % 10000 == 0):
					try:
						cursor.executemany(sql, each10000doc)
						# cursor.execute(sql, (each_doc))
						connection.commit()		
						each10000doc = [] # 
					except Exception as err:
						logger.error('error while executemany(): '+str(err))				
					logger.info('number of doc processed: '+str(count))
				# if (count > 10): # test 
				# 	break

		finally:	
			logger.info('length of last each10000doc: '+str(len(each10000doc)))
			try:
				cursor.executemany(sql, each10000doc)
				connection.commit()		
				each10000doc = [] # 
			except Exception as err:
				logger.error('error while executemany(): '+str(err))				
			logger.info('number of doc processed: '+str(count))					

except Exception as err:
	logger.error('error while cursor(): '+str(err))

finally:
	connection.close()	

logger.info('Done!')