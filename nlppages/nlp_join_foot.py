#!/root/anaconda3/envs/my_env/bin/python
import pymysql.cursors
import sys
import logging
import os.path
# import pandas as pd
# import numpy as np
import csv
# import re

# v1, not yet importing footprint

logger = logging.getLogger('nlp_join_foot')
logger.setLevel(logging.DEBUG)
filename, ext = os.path.splitext(os.path.basename(__file__))
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

# weboutcsv = 'web_df.csv'
# footoutcsv = 'foot_df.csv'
# joinoutcsv = 'join_df.csv'

logger.info('Start!')

# mysql breaktime -h localhost -u root -p
# Connect to the database
conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


	# (id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	# nlpid VARCHAR(100),
	# hostname VARCHAR(100),
	# title_lv1 VARCHAR(100),
	# title_lv2 VARCHAR(100),
	# title_lv3 VARCHAR(100),
	# title_k VARCHAR(100),
	# con_h1_k VARCHAR(100),
	# con_h2_k VARCHAR(100),
	# con_p_k VARCHAR(100),
	# con_k VARCHAR(100),
	# url VARCHAR(2083),
	# updated DATETIME,




cols = ['nlpid', 'hostname', 'title_lv1', 'title_lv2', 'title_lv3', 'title_k', 'con_h1_k', 'con_h2_k', 'con_p_k', 'con_k']
colstr = ','.join(cols)
tb = 'nlppages_20180131_20180417'
bigDict = {}
for idx, val in enumerate(cols):
	if idx > 1:
		# print(val)
		bigDict[val] = {} # {} nested under a big {}

def toDict(row):
	global bigDict
	for key in ['title_lv1', 'title_lv2', 'title_lv3', 'title_k', 'con_h1_k', 'con_h2_k', 'con_p_k', 'con_k']:
		for i in row[key].split():
			if i in bigDict[key]:
				bigDict[key][i] += 1
			else:
				bigDict[key][i] = 1		
# sys.exit()
try:
	count = 0
# first extract data from webpages
	with conn.cursor() as cursor:
		sql = "select "+colstr+" from "+tb+" where updated > '20180301' + INTERVAL 8 HOUR AND updated < '20180331' + INTERVAL 8 HOUR"
		logger.info(sql)		
		cursor.execute(sql)
		while True:
			rows = cursor.fetchmany(10000)
			count+= 10000
			logger.info('processed row count: '+str(count))
			if not rows:
				break
			for row in rows:
				pass
				toDict(row)
			# if count > 20000:
			# 	break

				

# # second, extract data from footprints_201803

# 	count = 0
# 	with conn.cursor() as cursor:
# 		# sql = "select pageid,count(id) as count from footprints_201803 group by pageid order by count desc limit 5"
# 		sql = "select pageid,count(id) as count from footprints_201803 group by pageid order by count desc "		
# 		cursor.execute(sql)
# 		while True:
# 			rows = cursor.fetchmany(10000)
# 			count+= 10000
# 			logger.info('processed footprint count: '+str(count))
# 			if not rows:
# 				break
# 			for row in rows:
# 				pass
# 				if row['pageid'] in pageidDict:
# 					footList.append([row['pageid'], row['count']])
except Exception as err:        
	logger.error(err)
finally:
    conn.close()
    # pageidDict.clear()

# top 10 keywords for each key
with open('dict.csv', 'w') as csv_file:
	writer = csv.writer(csv_file)
	for key1, val1 in bigDict.items():
		sortedKeys = sorted(val1, key=val1.get, reverse=True)
		count = 0
		for key2 in sortedKeys:
			if count >= 20:
				break
			writer.writerow([key1, key2, val1[key2]])
			count+=1	
		# for key2, val2 in val1.items():
			

# sys.exit()

logger.info('Done!')
