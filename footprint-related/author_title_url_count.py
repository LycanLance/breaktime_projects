#!/root/anaconda3/envs/my_env/bin/python
import sys
import pandas as pd
import os
sys.path.append('./')

# merge two files with url as index 
# usage:
# ./author_title_url_count.py author_title_url.csv  zimedia_url_list_201803.csv

# file1 = 'author_title_url.csv'
# file2 = 'zimedia_url_list_201803.csv'

file1 = sys.argv[1] # author_title_url.csv
file2 = sys.argv[2] # zimedia_url_list_201803.csv
# outfile = os.path.splitext(sys.argv[0])[0]+'.csv'
# outfile2 = os.path.splitext(sys.argv[0])[0]+'_.csv'
outfile = 'count_groupby_author_titlelv1.csv'
outfile2 = 'count_groupby_author.csv'

# print(outfile)
# sys.exit()
df1 = pd.read_csv(file1, low_memory=False)
df2 = pd.read_csv(file2, low_memory=False)

df1 = df1.iloc[:, 0:3]
df1.columns = ['author', 'title_lv1', 'url']
df2.columns = ['url', 'count']
df2['count'] = df2['count'].str.replace(',', '')  # important!

df3 = df1.join(df2.set_index('url'), on='url')
df3['count'].fillna(0, inplace=True)
df3['count'] = df3['count'].astype(int)

df4 = df3.groupby(['author','title_lv1'])['count'].sum()
# print(type(df4))
# df4.sort_values(['author', 'title_lv1', 'count'], ascending=[1,1,0] )

df4.to_csv('tmp')

df5 = pd.read_csv('tmp', header=None)
df5.columns = ['author', 'title_lv1', 'count']
os.remove('tmp')
# print(df5)
# print(df5.dtypes)
# df5.sort_values(by=['author', 'title_lv1', 'count'], ascending=[1,1,0], inplace=True)
df5.sort_values(by=['author','count'], ascending=[1,0], inplace=True)
# print(df5)
df5.to_csv(outfile, index=None)

df6 = df5.groupby(['author'])['count'].sum()
df6 = df6.sort_values(ascending=False)
df6.to_csv(outfile2)
# print(df6)
