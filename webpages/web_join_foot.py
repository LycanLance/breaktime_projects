#!/root/anaconda3/envs/my_env/bin/python
import pymysql.cursors
import sys
import logging
import os.path
import pandas as pd
import numpy as np
import re

logger = logging.getLogger('web_join_foot')
logger.setLevel(logging.DEBUG)
filename, ext = os.path.splitext(os.path.basename(__file__))
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

weboutcsv = 'web_df.csv'
footoutcsv = 'foot_df.csv'
joinoutcsv = 'join_df.csv'


def tostr(p_count):
	if p_count < 10: 
		p_range = 'below10'
	elif p_count >= 10 and p_count < 20:
		p_range = '10-20'
	elif p_count >= 20 and p_count < 30:
		p_range = '20-30'
	elif p_count >= 30 and p_count < 40:
		p_range = '30-40'
	else:
		p_range = 'above40'	
	return p_range	

# mysql breaktime -h localhost -u root -p
# Connect to the database
conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


webpagesList = []

# pageidtopcountDict = {}
# pageidList = []
pageidDict = {}
# pcountList = []
footList = []
try:
	count = 0
# first extract data from webpages
	with conn.cursor() as cursor:
		sql = "select webid,p_count,url from webpages_p order by p_count "		
		cursor.execute(sql)
		while True:
			rows = cursor.fetchmany(10000)
			count+= 10000
			logger.info('processed webpages count: '+str(count))
			if not rows:
				break
			for row in rows:
				pass
				if re.match(r'^\S+?/@\S+?/post/', row['url']):
					# pageidList.append(row['webid'])	
					pageidDict[row['webid']] = 1
					# pcountList.append(row['p_count'])
					p_range = tostr(row['p_count'])
					webpagesList.append([row['webid'], row['p_count'], p_range])
	# sys.exit()

# second, extract data from footprints_201803

	count = 0
	with conn.cursor() as cursor:
		# sql = "select pageid,count(id) as count from footprints_201803 group by pageid order by count desc limit 5"
		sql = "select pageid,count(id) as count from footprints_201803 group by pageid order by count desc "		
		cursor.execute(sql)
		while True:
			rows = cursor.fetchmany(10000)
			count+= 10000
			logger.info('processed footprint count: '+str(count))
			if not rows:
				break
			for row in rows:
				pass
				if row['pageid'] in pageidDict:
					footList.append([row['pageid'], row['count']])
except Exception as err:        
	logger.error(err)
finally:
    conn.close()
    pageidDict.clear()

webdf = pd.DataFrame(np.array(webpagesList), columns = ['webid', 'p_count', 'p_range'])
# print(weddf.head)
webdf.to_csv(weboutcsv, index=None)
# logger.info(type(webpagesList))
# logger.info(print(np.array(webpagesList).shape))
# logger.info('length of webpagesList: '+str(len(webpagesList)))

footdf = pd.DataFrame(np.array(footList), columns = ['pageid', 'count'])
footdf.to_csv(footoutcsv, index=None)


joindf = webdf.join(footdf.set_index('pageid'), on='webid')
joindf.to_csv(joinoutcsv, index=None)
logger.info(joindf.dtypes)
# logger.info(type(footList))
# logger.info('length of footList: '+str(len(footList)))

logger.info('Done!')
