#!/root/anaconda3/envs/my_env/bin/python
import pymysql.cursors
import sys
import logging
import os.path
import pandas as pd
import numpy as np
import matplotlib
# Force matplotlib to not use any Xwindows backend.
# this script is a mess
matplotlib.use('Agg')
import matplotlib.pyplot as plt

# v1, skip the plot part with param -f


logger = logging.getLogger('report')
logger.setLevel(logging.DEBUG)
filename, ext = os.path.splitext(os.path.basename(__file__))
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

# if '-p' in sys.argv:
# 	idx = sys.argv.index('-p')
# 	print(idx)
# else:
# 	print('do not plot')

# sys.exit()	
file = sys.argv[sys.argv.index('-in')+1] # author_title_url.csv
reportoutcsv = 'report_df.csv'
df = pd.read_csv(file)
# print(reportoutcsv)
# sys.exit()
# print(df.dtypes)
# webid       object
# p_count      int64
# count      float64
# dtype: object

# print(df.isnull().sum())
# webid          0
# p_count        0
# count      17062
# dtype: int64

df['count'].fillna(0, inplace=True)
df['count'] = df['count'].astype(int)
# df['p_range'] = df['p_range'].str()
# print(df.isnull().sum())
# print(df.dtypes)

peq0_df = df[df['p_count']==0]
# print(peq0_df)
# lenofsubdf = len(df[df['p_count']==0])
# print(lenofsubdf)
# sys.exit()

p_count_ds = df['p_count']
p_count_list = p_count_ds.tolist()
# print(type(p_count_ds.tolist()))
hist, bin_edges = np.histogram(p_count_list, bins=np.arange(100+1), density=False)
# print(len(hist))
# print(len(bin_edges[:-1]))
pcount_group_pv_sum_ds = df.groupby('p_count')['count'].sum()
# print(pcount_group_pv_sum_ds)
pcount_group_count_ds = df.groupby('p_count')['p_count'].count()
# print(pcount_group_count_ds)

prange_label = []
for i in pcount_group_count_ds.index:
	label = i//10 *10 
	# prange_label.append(str(label)+'-'+str(label+9))
	prange_label.append(str(label))
# print(prange_label)	
biz_df = pd.DataFrame({'<p>': pcount_group_count_ds.index, 'prange': prange_label, 'total_pages': pcount_group_count_ds, 'total_pv': pcount_group_pv_sum_ds}).reset_index(drop=True)


	# print(label) 
# print(biz_df['<p>'])

# sys.exit()
# biz_df = pd.concat([pcount_group_count_ds, pcount_group_pv_sum_ds], axis=1).reset_index()
# biz_df.loc['']
# print(biz_df)
biz_df.to_csv('biz_df.csv')
sys.exit()


if '-p' in sys.argv:
	# print(pcount_group_ds.index)
	# from 0 to max (step=1)
	pcount_group_sumcount_list = []
	for i in np.arange(100):
		if i in pcount_group_ds.index:
			pcount_group_sumcount_list.append(pcount_group_ds[i])
		else:
			pcount_group_sumcount_list.append(0)	

	# print(pcount_group_sumcount_list)
	# print(str(len(pcount_group_sumcount_list)))
	# pcount_group_list = pcount_group_ds.tolist()
	# print(pcount_group_list)
	# prange_count_ds.to_csv('prange_count_sum.csv')

	plotdf = pd.DataFrame({'x': bin_edges[:-1], 'p_count': hist, 'pageview': pcount_group_sumcount_list})
	# print(plotdf)	
	plotdf = plotdf.loc[:50,:]


	# N = 5
	# men_means = (20, 35, 30, 35, 27)
	# men_std = (2, 3, 4, 1, 2)

	# ind = np.arange(N)  # the x locations for the groups
	width = 0.5     # the width of the bars

	fig, ax = plt.subplots()
	# rects1 = ax.bar(ind, men_means, width, color='r', yerr=men_std)
	rects1 = ax.bar(plotdf.index, plotdf['p_count'], width, color='r')
	# women_means = (25, 32, 34, 20, 25)
	# women_std = (3, 5, 2, 3, 3)
	# rects2 = ax.bar(ind + width, women_means, width, color='y', yerr=women_std)
	rects2 = ax.bar(plotdf.index + width, plotdf['pageview'], width, color='y')
	# add some text for labels, title and axes ticks
	ax.set_ylabel('count')
	ax.set_title('count by p and pageview')

	plotdf.to_csv('biz_figure.csv')
	# print(plotdf.index)
	# print(type(plotdf.index))
	# xticks = []
	# for i in plotdf.index:
	# 	if (i % 5 == 0):
	# 		xticks.append(i)

	# print(xticks)
	# print(type(xticks))
	ax.set_xticks(plotdf.index + width / 2)
	plt.locator_params(axis='x', tight= True)
	matplotlib.ticker.MaxNLocator(nbins=10, integer=True)
	# ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))

	ax.legend((rects1[0], rects2[0]), ('p_count', 'pageview'))
	plt.grid(True)
	plt.savefig('phist_004')
	sys.exit()
	# print(str(len(hist)))
	# print(str(len(bin_edges)))
	# histdf = pd.DataFrame({'bin': bin_edges, 'p_hist': hist})
	# print(histdf.head())
	plt.figure(1)
	plt.hist(p_count_list, bins=np.arange(50+1))
	plt.xlabel('p_count')
	plt.ylabel('count')
	plt.title('Histogram of p_count')
	plt.axis([0, 50, 0, 50000])
	plt.grid(True)
	plt.savefig('phist_001')

	count_ds = df['count']
	count_ds = count_ds.sort_values(ascending=True)
	count_list = count_ds.tolist()
	# print(count_ds)
	# sys.exit()
	plt.figure(2)
	n , bins, patches = plt.hist(count_list, bins=[0, 2000, 4000, 6000, 8000, 10000])
	plt.xlabel('pageview')
	plt.ylabel('count')
	plt.title('Histogram of pageview')
	# plt.axis([0, 50, 0, 50000])
	plt.grid(True)
	plt.savefig('phist_002')
	print(str(n))
	print(str(bins))
	print(str(patches))

	plt.figure(3) # the other will be percentage

	plt.subplot(221)
	plt.hist(p_count_list, bins=np.arange(50+1))
	plt.xlabel('p_count')
	plt.ylabel('pageview')
	plt.title('Histogram of p_count')
	plt.axis([0, 50, 0, 50000])
	plt.grid(True)

	plt.subplot(222)
	plt.hist(p_count_list, bins=np.arange(50+1))
	plt.xlabel('p_count')
	plt.ylabel('pageview')
	plt.title('Histogram of p_count w/o 0')
	plt.axis([1, 50, 0, 50000])
	plt.grid(True)

	plt.subplot(223)
	plt.hist(p_count_list, bins=[0,10,20,30,40,50], density=True)
	plt.xlabel('p_count')
	plt.ylabel('pageview')
	plt.title('Histogram of p_count %')
	plt.axis([0, 50, 0, 0.06])
	plt.grid(True)

	plt.subplot(224)
	plt.hist(p_count_list, bins=[0,10,20,30,40,50], density=True)
	plt.xlabel('p_count')
	plt.ylabel('pageview')
	plt.title('Histogram of p_count % w/o 0')
	plt.axis([1, 50, 0, 0.06])
	plt.grid(True)

	# plt.subplots_adjust(top=0.9, bottom=0.1, left=0.15, right=0.85, hspace=0.30, wspace=0.35)
	plt.subplots_adjust(hspace=0.40, wspace=0.35)
	plt.savefig('phist_003')

df.sort_values(by=['p_range','count'], ascending=[1,0], inplace=True)
df = df.reset_index(drop=True)

# print(df.head())
# df.groupby('p_range').head(2).reset_index(drop=True)
prange_count_ds = df.groupby('p_range')['count'].sum()
# prange_count_ds.to_csv('prange_count_sum.csv')

prange_pcount_ds = df.groupby('p_range')['p_count'].count()
# prange_pcount_ds.to_csv('prange_pcount_count.csv')
bin10df = pd.DataFrame({'total_pageview': prange_count_ds, 'total_page': prange_pcount_ds})
# print(bin10df)
bin10df.to_csv('bin10df.csv')

df = df.groupby('p_range').head(2)
df = df.reset_index(drop=True)

# get the URL for those with p=0
conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


try:
	for i in range(len(peq0_df)):
		webid = peq0_df.loc[i, 'webid']
		# print(webid);

		with conn.cursor() as cursor:
			sql = "select url from webpages_p where webid =%s"
			cursor.execute(sql, (webid))
			rows = cursor.fetchall()
			tmp = []
			if not rows:
				break
			for row in rows:
				pass
				tmp.append(row['url'])
				# print(row)
			urlstr = '|'.join(tmp) # join with "|"
			peq0_df.loc[i, 'urlstr'] = urlstr
except Exception as ex:
	logger.error(ex)
finally:
	conn.close()	

peq0_df.sort_values(by=['count'], ascending=[0], inplace=True)
# print(peq0_df.head())
peq0_df.to_csv('peq0_df.csv', index=None)

sys.exit()
# print(df.dtypes)
# print(df.head())

conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)


try:
	for i in range(len(df)):
		webid = df.loc[i, 'webid']
		# print(webid);

		with conn.cursor() as cursor:
			sql = "select url from webpages_p where webid =%s"
			cursor.execute(sql, (webid))
			rows = cursor.fetchall()
			tmp = []
			if not rows:
				break
			for row in rows:
				pass
				tmp.append(row['url'])
				# print(row)
			urlstr = '|'.join(tmp) # join with "|"
			df.loc[i, 'urlstr'] = urlstr
except Exception as ex:
	logger.error(ex)
finally:
	conn.close()	

# print(df.head())
df.to_csv(reportoutcsv, index=None)

