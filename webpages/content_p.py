#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import datetime
import csv
import sys
import os
import pandas as pd
import numpy as np
import pymysql.cursors
import logging
import re
# import hashlib # decided to use pagid instead 
# v1, insert url and content_p data into mysql 
# change lt to lte
filename, ext = os.path.splitext(os.path.basename(__file__))


logger = logging.getLogger('content_p')
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)


logger.info('Start!')

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	# outstr = outlist.join(',')
	outstr = ','.join(outlist)	
	# print(outstr)	
	return outstr

'''=================== self-defined functions end ==================='''

argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')
startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]
indextb = argvs.index('-tb')
tablename = argvs[indextb+1]
indexesidx = argvs.index('-esidx')
esidx = argvs[indexesidx+1]
indexestype = argvs.index('-estype')
estype = argvs[indexestype+1]


# connection to ELK
esCluster = Elasticsearch(
    # hosts = ['192.168.21.20'],
    hosts = ['192.168.21.18'],
    http_auth = ('elastic', 'breaktime168'),
    port = 9200,
    timeout = 36000
)

# ========== the code below should be a worker ==========

# body = {
#   "_source": ["url", "content_p"],
#   "query": {
#     "bool": {
#       "must": [
#         {"term" : { "hostname" : "zi.media"}
#         },
#         {
#           "range": {
#             "updated": {
#               "gte": startDateString,
#               "lte": endDateString,
#               "time_zone": "+08:00",
#               "format": "yyyyMMdd"
#             }
#           }
#         }
#       ]
#     }
#   }
# }
body = {}
body['_source'] = ["url", "title", "content_p"]
body['query'] = {}
body['query']['bool'] = {}
body['query']['bool']['must'] = []
body['query']['bool']['must'].append({"term": {"hostname": "zi.media"}})
body['query']['bool']['must'].append({"range": { "updated": {"gte": startDateString, "lte": endDateString, "time_zone": "+08:00", "format": "yyyyMMdd"}}})


logger.info(body)
# print(body)
res = helpers.scan(
				client = esCluster,
				scroll = '20m',
				size = 10000,
				query = body,
				index = esidx,
				doc_type= estype,
				request_timeout = 5000
)

# mysql breaktime -h localhost -u root -p
# Connect to the database
connection = pymysql.connect(
		host='192.168.21.101',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)

datalist = ['webid','url', 'title', 'content_p', 'p_count']
insertcolstr = addacute(datalist)
sqlparam = ','.join(['%s'] * len(datalist))
# print(sqlparam)
# sys.exit()

# tablename = 'webpages_p'
count = 0

droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	webid VARCHAR(100),
	url VARCHAR(2083),
	title TEXT,
	content_p TEXT,
	p_count INT(100),
	KEY `webid` (`webid`)
	) '''
# md5_url VARCHAR(50)
# f773a12f3be73c737881ed86c92a57b2
# url VARCHAR(2083)	
# KEY `url` (`url`)	
# ENGINE=InnoDB DEFAULT CHARSET=utf8

each10000doc = []
try:
	with connection.cursor() as cursor:
		cursor.execute(droptbstr)
		logger.info(droptbstr)
		cursor.execute(createtbstr)
		logger.info(createtbstr)
		try:
			# while True: 
			for doc in res:
				# doc = res.next()
				# print(len(res))
				# print(doc)
				# break
				count+=1
				webid = doc['_id']
				url = ckattr(doc['_source'], 'url')
				# hash_object = hashlib.md5(url.encode())
				# md5_url = hash_object.hexdigest()
				title = ckattr(doc['_source'], 'title')
				content_p = ckattr(doc['_source'], 'content_p')
				# print(content_p)
				# matchObj = re.match(r"<p>.+?</p>", content_p, re.M|re.I)
				matchList = re.findall(r"<p>.+?</p>", content_p, re.M|re.I)
				# print(str(count)+': '+str(len(matchList)))
				# print(str(count)+': '+matchObj.group())
				# print(matchObj.keys())
				p_count = len(matchList)
				if p_count == 0:
					print(str(p_count)+','+content_p)
				# pageid = ckattr(doc['_source'], 'pageid')
				# fp = ckattr(doc['_source'], 'fp')
				# os_name = ckattr(doc['_source'], 'os_name')
				# name = ckattr(doc['_source'], 'name')
				# stay = ckattr(doc['_source'], 'stay')	
				# updated = doc['_source']['updated']
				each_doc = [webid, url, title, content_p, p_count]
				each10000doc.append(each_doc)
				sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"

				if (count % 10000 == 0):
					try:
						cursor.executemany(sql, each10000doc)
						# cursor.execute(sql, (each_doc))
						connection.commit()		
						each10000doc = [] # 
					except Exception as err:
						logger.error('error while executemany(): '+str(err))				
					logger.info('number of doc processed: '+str(count))
				# if (count > 10): # test 
				# 	break

		finally:	
			logger.info('length of last each10000doc: '+str(len(each10000doc)))
			try:
				cursor.executemany(sql, each10000doc)
				connection.commit()		
				each10000doc = [] # 
			except Exception as err:
				logger.error('error while executemany(): '+str(err))				
			logger.info('number of doc processed: '+str(count))					

except Exception as err:
	logger.error('error while cursor(): '+str(err))

finally:
	connection.close()	

logger.info('Done!')