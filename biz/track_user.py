'''
stat the daily wifi pv among a time range
v1, aim at calculating the nearest fp footprint. postponed
'''

from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta, datetime
import csv
import sys
import os
import pandas as pd
import logging
import multiprocessing as mp
sys.path.append('/home/lance/tools/')
from common.logger import logger
# import pandas as pd

params = ['S', 'E', 'out']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

# esidx = paramDic['esidx']
# estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
# qpath = paramDic['path']
label = paramDic['out']
# outfile = paramDic['out']

filepath = os.path.abspath(__file__)
logger = logger(filepath)

logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    if attr in po.keys():
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def toDateObj(indate):
    # from 20180301 to 2018-03-01
    # print(indate)
    return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:8]))
    # return datetime(int(indate[0:4]), int(indate[4:6]), int(indate[6:8]), hour=int(indate[8:10]), minute=int(indate[10:12]), second=int(indate[12:14]))
    # return date[0:4]+'-'+date[4:6]+'-'+date[6:]


'''=================== self-defined functions end ==================='''

pathlist = ['/event/hcnicecloud/', '/event/hc-nice-hytqbest/',
            '/event/allianz', '/event/jec/']
q_list = pathlist
start = toDateObj(startDateString)
end = toDateObj(endDateString)


# a = '2018-05-15T12:36:42'
# b = '2018-05-15T12:59:42'
# aObj = datetime.strptime(a, '%Y-%m-%dT%H:%M:%S')
# bObj = datetime.strptime(b, '%Y-%m-%dT%H:%M:%S')
# print(aObj)
# print(bObj)
# print(aObj-bObj)
# print(bObj-aObj)
# # print(type(tObj))
# # d, t = a.split('T')
# # print(d, t)

# sys.exit()
# q_date = []
# while (start <= end):
#     # print(start)
#     q_date.append(str(start))
#     start = start + timedelta(days=1)

# print(q_date)
# print(start)
# sys.exit()
proc = 8
stepsize = 1
# stepsize = len(
#     qrange) // proc if len(qrange) % proc < proc // 2 else len(qrange) // proc + 1  # 4
# print(stepsize)
# sys.exit()

# connection to ELK
esCluster = Elasticsearch(
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000
)


def worker(stepnum):
    '''
    prepare fingerprint candidata
    '''
    pass
    global q_list
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # returnlist = []
    retDict = {}
    for each in todoworklist:
        retDict[each] = []

        body = {}
        body['_source'] = ["path", "fp", "updated"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['filter'].append(
            {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": str(start), "lte": str(end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        body['query']['bool']['must'].append(
            {"term": {"path": each}})
        logger.info(body)

        # res = esCluster.search(
        #     index="footprints_2018*",
        #     doc_type="footprint",
        #     scroll="3m",
        #     search_type="query_then_fetch",
        #     size=0,
        #     body=body, request_timeout=300)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # total = res['hits']['total']
        # retDict[each][wifi] = total
        # print(each, wifi, total)
        # sys.exit()
        count = 0
        # outlist = []
        for doc in res:
            fp = doc['_source']['fp']
            # path = doc['_source']['path']
            ts = doc['_source']['updated']
            # to speed up the process, don't do it yet
            # ts = str(datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S'))
            # print(datetime(ts))
            # outlist = [fp, path, ts]
            # print(outlist)
            retDict[each].append((fp, ts))
            count += 1
            if count == 5:
                break
        logger.info('num of processed doc: ' + str(count))

    return retDict


def getExtendedFpInfo(stepnum):
    '''
    get the previous and the next footprint of the candidate fingerprint in footprints
    '''
    global q_list
    # list of tuple (fp, datetime), e.g. (720c22f395a9fea66aaa3ee55b22d4a3,2018-05-28T15:17:07)
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # returnlist = []
    retDict = {}
    for eachfp, eachts in todoworklist:
        tObj = datetime.strptime(eachts, '%Y-%m-%dT%H:%M:%S')
        start = tObj - timedelta(minutes=30)
        end = tObj + timedelta(minutes=30)
        body = {}
        body['_source'] = ["url", "fp", "updated"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['filter'].append(
            {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": str(start), "lte": str(end), "format": "yyyy-MM-dd HH:mm:ss"}}})
        # "time_zone": "+08:00",
        body['query']['bool']['must'].append(
            {"term": {"fp": eachfp}})

        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        count = 0

        fpDict = {}
        fpDict['fp'] = eachfp
        fpDict['prev'] = ''
        fpDict['next'] = ''
        pdist = timedelta()
        ndist = timedelta()

        for doc in res:
            ts = doc['_source']['updated']
            url = doc['_source']['url']
            if ts == eachts:  # same as candidate, skip!
                continue
            doctsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
            if tObj > doctsObj:
                pass
                tmpdist = tObj - doctsObj
                if fpDict['prev'] == '':
                    fpDict['prev'] = (ts, url)
                    pdist = tmpdist
                if tmpdist < pdist:
                    fpDict['prev'] = (ts, url)
                    pdist = tmpdist

            elif tObj < doctsObj:
                pass
                # next
                tmpdist = doctsObj - tObj
                if fpDict['next'] == '':
                    fpDict['next'] = (ts, url)
                    ndist = tmpdist
                if tmpdist < pdist:
                    fpDict['next'] = (ts, url)
                    ndist = tmpdist
            count += 1
            # if count == 1:
            #     break
        # print(str(tObj), fpDict)
        logger.info('num of processed doc for ' + eachfp + ': ' + str(count))


def multiproc():
    pass
    global q_list
    procc = proc if proc <= len(q_list) else len(q_list)
    with mp.Pool() as pool:
        res = pool.map(worker, range(procc))

    # merge dicts
    joinDict = {}
    for eachdict in res:
        # print(eachdict)
        joinDict.update(eachdict)

    # simple stat
    # for k, il in joinDict.items():
    #     print(k, str(len(il)))

    # print(joinDict)
    outfile = 'data/' + label + '.csv'
    with open(outfile, 'w') as csvf:
        writer = csv.writer(csvf)
        writer.writerow(['path', 'fp', 'ts'])
        for k, il in joinDict.items():
            for it in il:
                outlist = []
                outlist.append(k)
                outlist.append(it[0])
                outlist.append(it[1])
                writer.writerow(outlist)

    for k, il in joinDict.items():
        procc = proc if proc <= len(il) else len(il)
        q_list = il
        with mp.Pool() as pool:
            res = pool.map(getExtendedFpInfo, range(procc))


if __name__ == '__main__':
    multiproc()
    logger.info('Done!')
