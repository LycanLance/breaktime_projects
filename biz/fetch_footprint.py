#!/root/anaconda3/envs/my_env/bin/python
from elasticsearch import Elasticsearch, helpers
import pymysql.cursors
import sys
import logging
import os.path
# import pandas as pd
# import numpy as np
import csv
import re
import multiprocessing as mp
# date: 20180430
# v1, merge the footprint info 
# hanging, working on dashboard

params = ['intb', 'proc', 'host', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'esidx', 'estype', 'S', 'E', 'out']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
	if '-'+param in argvs:
		idx = argvs.index('-'+param)
		paramDic[param] =  argvs[idx+1] # check the data type
	# else:
	# 	paramDic[param] = ''	

# print(paramDic)
# sys.exit()
if (paramDic['proc'] != ''):
	numprocess = int(paramDic['proc'])
if (paramDic['esidx'] != ''):
	esidx = paramDic['esidx']
if (paramDic['estype'] != ''):
	estype = paramDic['estype']
if (paramDic['out'] != ''):
	outputlabel = paramDic['out']
# restrict the search on footprint_201803*	
startDateString = paramDic['S']
endDateString = paramDic['E']


filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename + '.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('cmdline: '+' '.join(sys.argv))
logger.info('Start!')

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return 'NA'


'''=================== self-defined functions end ==================='''

pageidInfo = {}
# select from sql 
try:
	conn = pymysql.connect(host='localhost',
                             user='lance',
                             password='',
                             db='breaktime',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

	cols = ['pageid', 'url','hostname','author','title','meta_desc']
	colstr = ','.join(cols)
	tb = paramDic['intb']
	todopageidList = []
	with conn.cursor() as cursor:
		wherelist = []
		if 'host' in paramDic and paramDic['host'] != '':
			matchObj = re.match(r'^-(\S+)', paramDic['host'])
			wherelist.append("hostname != '"+matchObj.group(1)+"'") if matchObj else wherelist.append("hostname = '"+paramDic['host']+"'")
		for x in ['a','b','c','d','e','f','g','h']:
			if x not in paramDic:
				continue
			# print(x)
			# print(type(x))	
			# print(paramDic[x])
			wherelist.append(x+' > 0') if (int(paramDic[x]) > 0) else wherelist.append(x+' = 0')	

		whereclause = " where "+' AND '.join(wherelist)
		sql = "select "+colstr+" from "+tb+whereclause
		# sql = "select "+colstr+" from "+tb+" limit 1000"
		logger.info(sql)	
		try:
			cursor.execute(sql)
		except Exception as e:
			logger.error('error_1: '+str(e))

		try:
			while True:
				rows = cursor.fetchmany(10000)
				if not rows:
					break
				for row in rows:
					pageidInfo[row['pageid']] = {}
					for i in ['url','hostname','author','title','meta_desc']:
						pageidInfo[row['pageid']][i] = row[i]
					todopageidList.append(row['pageid'])
		except Exception as e:
			logger.error('error_2: '+str(e))			


except Exception as e:
	logger.error('error_3: '+str(e))

# print(pageidInfo)
# print(todopageidList)
logger.info('number of total pageid: '+str(len(todopageidList)))
# sys.exit()
interestedKeys = ['age', 'gender', 'device', 'os']
mapping = {
	"age": "wlan_userage",
	"gender": "wlan_usergender",
	"device": "device_type",
	"os": "os"
}

def worker(stepnum):
	# print(stepnum)
	if len(todopageidList) >= numprocess:
		step = len(todopageidList)//numprocess
		worker_start = 0 + step*stepnum
		if stepnum == numprocess-1:
			worker_end = len(todopageidList)
		else:
			worker_end = worker_start + step
	else:
		logger.debug('proc setting should not exceed the number of len of todopageidList')
		sys.exit()
		worker_start = 0
		worker_end = len(todopageidList)

	# logger.info([worker_start, worker_end])	
	worklist = todopageidList[worker_start:worker_end]

	esCluster = Elasticsearch(
	    # hosts = ['192.168.21.20'],
	    hosts = ['192.168.21.18'],
	    http_auth = ('elastic', 'breaktime168'),
	    port = 9200,
	    timeout = 36000
	)

	pageidcount = 0
	logger.info('len of worklist: '+str(len(worklist)))
	pageidDict = {}
	for eachpageid in worklist:

		body = {}
		body['_source'] = ["fp", "wlan_userage", "wlan_usergender", "userage", "usergender", "path", "device_type", "os"]
		body['query'] = {}
		body['query']['bool'] = {}
		body['query']['bool']['must'] = []
		body['query']['bool']['filter'] = []	
		body['query']['bool']['must'].append({"term": {"pageid": eachpageid}})
		body['query']['bool']['filter'].append({"range": { "updated": {"gte": startDateString, "lte": endDateString, "time_zone": "+08:00", "format": "yyyyMMdd"}}})

		res = helpers.scan(
						client = esCluster,
						scroll = '20m',
						size = 10000,
						query = body,
						index = esidx,
						doc_type= estype,
						request_timeout = 5000
		)	
		count = 0
		fpset = set()

		if eachpageid not in pageidDict:
			pageidDict[eachpageid] = {}
			pageidDict[eachpageid]['count'] = 0
			pageidDict[eachpageid]['fp'] = 0
			for i in interestedKeys:
				pageidDict[eachpageid][i] = {}
		for doc in res:
			pageidDict[eachpageid]['count'] += 1
			fp = ckattr(doc['_source'], 'fp')
			fpset.add(fp)
			pageidDict[eachpageid]['fp'] = len(fpset)
			# print(str(len(fpset)))
			for i in interestedKeys:
				elkres = ckattr(doc['_source'], mapping[i])
				if elkres == '':
					elkres = 'NA'
				if elkres in pageidDict[eachpageid][i]:
					pageidDict[eachpageid][i][elkres] += 1
				else:		
					pageidDict[eachpageid][i][elkres] = 1

			# age = ckattr(doc['_source'], 'wlan_userage')
			# gender = ckattr(doc['_source'], 'wlan_usergender')
			# device = ckattr(doc['_source'], 'device_type')
			# os = ckattr(doc['_source'], 'os')
			# print([age, gender, device, os])
			# for i in [age, gender, device, os]:
			count += 1
			if not (count % 10000):
				logger.info('num of processed doc: '+str(count))
		

		pageidcount += 1
		if not (pageidcount % 100):
			logger.info('num of processed pageid: '+str(pageidcount))
			


	return pageidDict



if __name__ == '__main__':
	if len(todopageidList) < numprocess:
		numprocess = len(todopageidList)
	# print(str(numprocess))
	# sys.exit()
	with mp.Pool() as pool:
		res = pool.map(worker, range(numprocess))

	# print(res)
	mres = {}
	for i in res:
		mres = dict(mres, **i)

	tmp = {}
	for x in mres:
		tmp[x] = mres[x]['count']
	sortedk = sorted(tmp, key=tmp.get, reverse=True)	
	# print(mres)
	# sys.exit()
	# output file 1
	outDict = {}
	outDict['count'] = 0
	outDict['fp'] = 0
	for key in interestedKeys:
		if key not in outDict:
			outDict[key] = {}
	outfile = 'data/'+outputlabel+'pageid.csv'
	with open(outfile, 'w') as f:
		writer = csv.writer(f)
		# ['url','hostname','author','title','meta_desc']
		writer.writerow(['pageid','url','hostname','author','title','meta_desc','pv','fp','age', 'gender', 'device_type', 'os'])
		# for eachdic in res:
		# sortedk = sorted(mres, key=mres['count'].get, reverse=True)
		# for eachpageid in mres.keys():
		for eachpageid in sortedk:	
			outputlist = []
			outputlist = [eachpageid]
			for i in ['url','hostname','author','title','meta_desc']:
				outputlist.append(pageidInfo[eachpageid][i])
			outputlist.append(mres[eachpageid]['count'])
			# try:
			outputlist.append(mres[eachpageid]['fp'])
			# except:
			# 	print('debug: '+eachpageid)
			# 	print('debug_2: '+mres[eachpageid])
			outDict['count'] += mres[eachpageid]['count']
			outDict['fp'] += mres[eachpageid]['fp']
			for key in interestedKeys:
				sortedk = sorted(mres[eachpageid][key], key=mres[eachpageid][key].get, reverse=True)
				klist = []
				for k in sortedk:
					klist.append(k+':'+str(mres[eachpageid][key][k]))
					if k in outDict[key]:
						outDict[key][k] += mres[eachpageid][key][k]
					else:
						outDict[key][k] = mres[eachpageid][key][k]
				kstr = ';'.join(klist)
				outputlist.append(kstr)
			writer.writerow(outputlist)
					
	# print(outDict)
	# output file 2
	outfile = 'data/'+outputlabel+'whole.csv'
	with open(outfile, 'w') as f:
		writer = csv.writer(f)
		# count = 0
		# for key in outDict:
		# 	if key == 'count': continue
		# writer.writerow(['pv','fp','age', 'gender', 'device_type', 'os'])
		writer.writerow(['pv','age', 'gender', 'device_type', 'os'])
		outputlist = [outDict['count']]
		# outputlist.append(outDict['fp'])
		for k in interestedKeys:
			klist = []
			sortedk = sorted(outDict[k], key=outDict[k].get, reverse=True)
			for x in sortedk:
				klist.append(x+':'+str(outDict[k][x]))
			kstr = ';'.join(klist)
			outputlist.append(kstr)
		writer.writerow(outputlist)
			# if count < 10:
			# 	print(i+','+str(Dict[i]))
			# count+=1 

logger.info('Done!')
