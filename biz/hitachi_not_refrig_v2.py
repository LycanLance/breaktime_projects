#!/root/anaconda3/envs/my_env/bin/python
# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta
import csv
import sys
import os
# import pandas as pd
# import numpy as np
import pymysql.cursors
import logging
import multiprocessing as mp

# import json
import re
# import hashlib # decided to use pagid instead 
# v1, get data of interest from index:webpages and store them to sql
# v2, store into sql

argvs = sys.argv[1:]
indexStart = argvs.index('-S')
indexEnd = argvs.index('-E')
startDateString = argvs[indexStart+1]
endDateString = argvs[indexEnd+1]
indextb = argvs.index('-tb')
tablename = argvs[indextb+1]
indexproc = argvs.index('-proc')
numprocess = int(argvs[indexproc+1])
# indexesidx = argvs.index('-esidx')
# esidx = argvs[indexesidx+1]
# indexestype = argvs.index('-estype')
# estype = argvs[indexestype+1]
# indexaggsize = argvs.index('-aggsize')
# aggs_size = argvs[indexaggsize+1]
esidx = 'nlppages'
estype = 'nlppage'

filename, ext = os.path.splitext(os.path.basename(__file__))
logger = logging.getLogger(filename)
logger.setLevel(logging.DEBUG)
fh = logging.FileHandler(filename+'.log')
fh.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(process)d - %(message)s')
ch.setFormatter(formatter)
fh.setFormatter(formatter)
logger.addHandler(ch)
logger.addHandler(fh)

logger.info('cmdline: '+' '.join(sys.argv))
logger.info('Start!')

'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
	if attr in po.keys():
		# print(po[attr])
		return po[attr]
	else:
		if attr == 'stay':
			return 0
		else:	
			return ''

def addacute(a):
	outlist = []
	for i in a:
		iaccute = '`'+i+'`' 
		outlist.append(iaccute)
	outstr = ','.join(outlist)	
	return outstr

def toDateObj(indate):
	# from 20180301 to 2018-03-01
	return date(int(indate[0:4]), int(indate[4:6]), int(indate[6:]))

def initwordDic():
	# total 8 elements
	# [a,b,c,d,e,f,g,h]
	wordlist = ['日立', '冰箱', '洗衣機', '烘烤微波爐', '吸塵器', '投影機', '家電', '冷氣']
	wordDic = {}
	for eachword in wordlist:
		wordDic[eachword] = 0
	return wordlist, wordDic

def toSQL(cursor, tablename, each10000list):
# [pageid, hostname, title, titleDic, meta_desc, metaDic]
	datalist = ['pageid', 'url', 'hostname', 'author', 'title', 'title_stat', 'meta_desc', 'meta_stat', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
	insertcolstr = addacute(datalist)
	sqlparam = ','.join(['%s'] * len(datalist))

	sql = "INSERT INTO "+tablename+" ("+insertcolstr+") VALUES ("+sqlparam+")"
	cursor.executemany(sql, each10000list)



'''=================== self-defined functions end ==================='''


start = toDateObj(startDateString)
end = toDateObj(endDateString)
delta = end - start
step = delta//numprocess

conn = pymysql.connect(
		host='localhost',
		user='lance',
		password='',
		db='breaktime',
		charset='utf8mb4',
		cursorclass=pymysql.cursors.DictCursor
)

# tablename = 'hitachi_not_refrig'
droptbstr = '''DROP TABLE IF EXISTS '''+tablename
createtbstr = '''CREATE TABLE IF NOT EXISTS '''+tablename+'''
	(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	pageid VARCHAR(40) NOT NULL,
	url TEXT,
	hostname VARCHAR(100),
	author TEXT,
	title TEXT,
	title_stat TEXT,
	meta_desc TEXT,
	meta_stat TEXT,
	a INT UNSIGNED DEFAULT 0,
	b INT UNSIGNED DEFAULT 0,
	c INT UNSIGNED DEFAULT 0,
	d INT UNSIGNED DEFAULT 0,
	e INT UNSIGNED DEFAULT 0, 
	f INT UNSIGNED DEFAULT 0,
	g INT UNSIGNED DEFAULT 0,
	h INT UNSIGNED DEFAULT 0,
	KEY `pageid` (`pageid`),
	KEY `hostname` (`hostname`),
	KEY `a` (`a`),
	KEY `b` (`b`),
	KEY `c` (`c`),
	KEY `d` (`d`),
	KEY `e` (`e`),
	KEY `f` (`f`),
	KEY `g` (`g`),
	KEY `h` (`h`)
	) '''

try:
	with conn.cursor() as cursor:
		cursor.execute(droptbstr)
		cursor.execute(createtbstr)
except Exception as e:
	logger.error(e)		


def worker(stepnum):

	worker_start = start + step*stepnum
	if stepnum == numprocess-1:
		worker_end = end
	else:
		worker_end = worker_start + step - timedelta(days=1)

	body = {}
	body['_source'] = ["url", "hostname", "title", "meta_description", "author_name"]
	# body['size'] = 0
	body['query'] = {}
	body['query']['bool'] = {}
	body['query']['bool']['must'] = []
	body['query']['bool']['filter'] = []
	body['query']['bool']['must_not'] = []
	body['query']['bool']['should'] = []	
	# if '-zi' in sys.argv: # params for zi.media only
	# 	body['query']['bool']['must'].append({"term": {"hostname": "zi.media"}})
	body['query']['bool']['should'].append({"multi_match": {"query": "日立", "type": "phrase", "fields": ["title", "meta_description"]}})
	body['query']['bool']['should'].append({"multi_match": {"query": "冰箱", "type": "phrase", "fields": ["title", "meta_description"]}})
	body['query']['bool']['should'].append({"multi_match": {"query": "洗衣機", "type": "phrase", "fields": ["title", "meta_description"]}})
	body['query']['bool']['should'].append({"multi_match": {"query": "烘烤微波爐", "type": "phrase", "fields": ["title", "meta_description"]}})
	body['query']['bool']['should'].append({"multi_match": {"query": "吸塵器", "type": "phrase", "fields": ["title", "meta_description"]}})	
	body['query']['bool']['should'].append({"multi_match": {"query": "投影機", "type": "phrase", "fields": ["title", "meta_description"]}})
	body['query']['bool']['should'].append({"multi_match": {"query": "家電", "type": "phrase", "fields": ["title", "meta_description"]}})
	body['query']['bool']['must_not'].append({"multi_match": {"query": "冷氣", "type": "phrase", "fields": ["title", "meta_description"]}})	
	body['query']['bool']['filter'].append({"range": {"updated": {"gte": str(worker_start), "lte": str(worker_end), "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})	
	# this has no effect
	body['query']['bool']['filter'].append({"regexp": {"author_name": ".*"}})

	logger.info(body)

	esCluster = Elasticsearch(
	    # hosts = ['192.168.21.20'],
	    hosts = ['192.168.21.18'],
	    http_auth = ('elastic', 'breaktime168'),
	    port = 9200,
	    timeout = 36000
	)
	res = helpers.scan(
					client = esCluster,
					scroll = '20m',
					size = 10000,
					query = body,
					index = esidx,
					doc_type= estype,
					request_timeout = 5000
	)	
	# res = esCluster.search(
	# 	index=esidx,
	# 	doc_type=estype,
	# 	# scroll="3m",
	# 	search_type="query_then_fetch",
	# 	# size=scroll_size,
	# 	body=body,
	# 	request_timeout=3600
	# 	)
	conn = pymysql.connect(
			host='localhost',
			user='lance',
			password='',
			db='breaktime',
			charset='utf8mb4',
			cursorclass=pymysql.cursors.DictCursor
	)	

	count = 0
	outlist = []
	each10000list = []
	# outDic = {}
	try: 
		with conn.cursor() as cursor:
			try:
				for doc in res:
					count+=1		
					pageid = doc['_id']
					url = ckattr(doc['_source'], 'url')
					hostname = ckattr(doc['_source'], 'hostname')
					author_name = ckattr(doc['_source'], 'author_name')
					if hostname == 'zi.media' and author_name == '':
						continue
					title = ckattr(doc['_source'], 'title')
					meta_desc = ckattr(doc['_source'], 'meta_description')
					titlelist, titleDic = initwordDic()
					metalist, metaDic = initwordDic()
					wordcountlist = []
					check = 0
					for eachword in titlelist: # read in with the correct order
						eachTitleMatchList = re.findall(eachword, title)
						eachMetaMatchList = re.findall(eachword, meta_desc)
						titleDic[eachword] = len(eachTitleMatchList)
						metaDic[eachword] = len(eachMetaMatchList)
						if (len(eachTitleMatchList)+len(eachMetaMatchList) > 0 ):check = 1 
						wordcountlist.append(len(eachTitleMatchList)+len(eachMetaMatchList))
					if check == 0:
						continue	
					titleDicList = []
					metaDicList = []
					# for key in sorted(titleDic, key=titleDic.get, reverse=True):
					for key in titlelist:
						titleDicList.append(key+':'+str(titleDic[key]))
					# for key in sorted(metaDic, key=metaDic.get, reverse=True):
					for key in metalist:
						metaDicList.append(key+':'+str(metaDic[key]))
					eachlist = [pageid, url, hostname, author_name, title, ','.join(titleDicList), meta_desc, ','.join(metaDicList)]
					eachlist.extend(wordcountlist)
					each10000list.append(eachlist)
					# print(outlist)
					if (count % 10000 == 0):
						toSQL(cursor, tablename, each10000list)
						conn.commit()
						each10000list = []
						logger.info('number of doc processed: '+str(count))
					# if count > 1:
					# 	break
			finally:
				toSQL(cursor, tablename, each10000list)
				conn.commit()
	except Exception as e:
		logger.error(e)
	finally:
		conn.close()	

	logger.info('number of doc processed: '+str(count))			
	logger.info('Child proc done!')
	return str(count)

if __name__ == '__main__':
	# pool = mp.Pool()
	with mp.Pool() as pool:
		res = pool.map(worker, range(numprocess))

	logger.info('to SQL done!')
	# print(str(len(res)))	
	# print(res)
	# for eachd in res:
	# 	print(eachd)
	logger.info('Done!')
	sys.exit()

