'''
fetch fp within 5/27~6/3 (input from cmd)
observe the user behavior within 5/14~6/3 (hard coded)
stat the daily wifi pv among a time range
v1, aim at calculating the nearest fp footprint. postponed!
v2, not the nearest but all within the time range
v2.1, change secdiff to mindiff. add title
'''

from elasticsearch import Elasticsearch, helpers
from datetime import date, timedelta, datetime
import csv
import sys
import os
import pandas as pd
import logging
import multiprocessing as mp
from operator import itemgetter
sys.path.append('/home/lance/tools/')
from common.logger import logger
filepath = os.path.abspath(__file__)
logger = logger(filepath)

params = ['S', 'E', 'out']
# q_list = []
argvs = sys.argv
# print(argvs)
paramDic = {}
for param in params:
    if '-' + param in argvs:
        idx = argvs.index('-' + param)
        paramDic[param] = argvs[idx + 1]
    else:
        err = 1
        print('param -' + param + ' is missing!')
try:
    if err:
        sys.exit(1)
except Exception as e:
    pass

# esidx = paramDic['esidx']
# estype = paramDic['estype']
startDateString = paramDic['S']
endDateString = paramDic['E']
# qpath = paramDic['path']
label = paramDic['out']
# outfile = paramDic['out']


logger.info('cmdline: ' + ' '.join(sys.argv))
logger.info('Start!')


'''=================== self-defined functions start ==================='''


def ckattr(po, attr):
    # try replace with doc['_source'].get('title', '') next time
    # if hasattr(po, attr)
    if attr in po.keys():
        return po[attr]
    else:
        if attr == 'stay':
            return 0
        else:
            return ''


def addacute(a):
    outlist = []
    for i in a:
        iaccute = '`' + i + '`'
        outlist.append(iaccute)
    outstr = ','.join(outlist)
    return outstr


def dtdelta2min(td):
    s = timedelta.total_seconds(td)
    m = s / 60
    return f'{m:.2f}'  # rounded to second decimal place
    # td = f'{int(td)}'


'''=================== self-defined functions end ==================='''

pathlist = ['/event/hcnicecloud/', '/event/hc-nice-hytqbest/',
            '/event/allianz', '/event/jec/']
q_list = pathlist
start = datetime.strptime(startDateString, '%Y%m%d').strftime('%Y-%m-%d')
end = datetime.strptime(endDateString, '%Y%m%d').strftime('%Y-%m-%d')

proc = 8
stepsize = 1

# connection to ELK
esCluster = Elasticsearch(
    hosts=['192.168.21.11'],
    # http_auth=('elastic', 'breaktime168'),
    port=9200,
    timeout=36000
)


def worker(stepnum):
    '''
    prepare fingerprint candidata
    '''

    global q_list
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # returnlist = []
    retDict = {}
    for each in todoworklist:
        retDict[each] = []

        body = {}
        body['_source'] = ["geoip.ip", "fp",
                           "updated", "userage", "usergender", "openlink"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        body['query']['bool']['filter'].append(
            {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": start, "lte": end, "time_zone": "+08:00", "format": "yyyy-MM-dd"}}})
        body['query']['bool']['must'].append(
            {"term": {"path": each}})
        logger.info(body)

        # res = esCluster.search(
        #     index="footprints_2018*",
        #     doc_type="footprint",
        #     scroll="3m",
        #     search_type="query_then_fetch",
        #     size=0,
        #     body=body, request_timeout=300)
        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        # total = res['hits']['total']
        # retDict[each][wifi] = total
        # print(each, wifi, total)
        # sys.exit()
        count = 0
        # outlist = []
        for doc in res:
            # print(doc)
            fp = doc['_source']['fp']
            # path = doc['_source']['path']
            ip = doc['_source']['geoip']['ip']
            ts = doc['_source']['updated']
            sex = ckattr(doc['_source'], 'usergender')
            age = ckattr(doc['_source'], 'userage')
            oplk = ckattr(doc['_source'], 'openlink')  # list
            # oplk_tuple = ()  # external, href, text
            oplk_list = []
            if oplk == '':
                pass
                # oplk_tuple = ('', '', '', '')
            else:
                ol_len = len(oplk)
                for tmpdict in oplk:
                    external = ckattr(tmpdict, 'external')
                    href = ckattr(tmpdict, 'href')
                    text = ckattr(tmpdict, 'text')
                    oplk_tuple = (external, href, text)
                    oplk_list.append(oplk_tuple)

            # to speed up the process, don't do it yet
            # ts = str(datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S'))
            # print(datetime(ts))
            # outlist = [fp, path, ts]
            # print(outlist)
            retDict[each].append((fp, ip, ts, sex, age, oplk_list))
            count += 1
            # if count == 5:
            #     break
        logger.info('num of processed doc: ' + str(count))

    return retDict


def getExtendedFpInfo(stepnum):
    '''
    get the previous and the next footprint of the candidate fingerprint in footprints
    '''

    # global q_list
    # list of tuple (fp, datetime), e.g. (720c22f395a9fea66aaa3ee55b22d4a3,2018-05-28T15:17:07)
    todoworklist = []
    if stepnum == proc - 1:  # last worker do all the rest
        todoworklist = q_list[0 + stepsize * stepnum:]
    else:
        todoworklist = q_list[0 + stepsize *
                              stepnum:(0 + stepsize * stepnum) + stepsize]
    # print(todoworklist)
    # sys.exit()
    # retList = []
    retDict = {}
    for eachtuple in todoworklist:
        eachfp, eachip, eachts, eachsex, eachage, eachol_list = eachtuple
        # ol_len, external, href, text = eachol_tp
        keytuple = (eachfp, eachip, eachts)
        tObj = datetime.strptime(eachts, '%Y-%m-%dT%H:%M:%S')
        # start = tObj - timedelta(minutes=30)
        # end = tObj + timedelta(minutes=30)
        # logger.info('The candidate')
        # logger.info(keytuple)
        # rangedtformat = "yyyy-MM-dd HH:mm:ss"
        start = "2018-05-14"
        end = "2018-06-03"
        rangedtformat = "yyyy-MM-dd"  # by day
        body = {}
        body['_source'] = ["path", "url", "geoip.ip",
                           "pageid", "fp", "updated", "title"]
        # body['size'] = 0
        body['query'] = {}
        body['query']['bool'] = {}
        body['query']['bool']['must'] = []
        body['query']['bool']['must_not'] = []
        body['query']['bool']['filter'] = []
        # body['query']['bool']['filter'].append(
        #     {"term": {"hostname": "zi.media"}})
        body['query']['bool']['filter'].append({"range": {"updated": {
                                               "gte": start, "lte": end, "time_zone": "+08:00", "format": rangedtformat}}})
        # "time_zone": "+08:00",
        body['query']['bool']['filter'].append(
            {"term": {"fp": eachfp}})
        body['query']['bool']['filter'].append(
            {"term": {"geoip.ip": eachip}})

        res = helpers.scan(
            client=esCluster,
            scroll='20m',
            size=10000,
            query=body,
            index='footprints_2018*',
            doc_type='footprint',
            request_timeout=5000
        )
        count = 0

        eachDict = {}
        retDict[keytuple] = {}
        eachDict = retDict[keytuple]
        # eachDict['candidate'] = keytuple # tuple
        eachDict['sex'] = eachsex
        eachDict['age'] = eachage
        eachDict['openlink'] = eachol_list
        eachDict['prev'] = []  # list
        eachDict['next'] = []  # list
        eachDict['bself'] = 0
        eachDict['aself'] = 0
        # pdist = timedelta()
        # ndist = timedelta()

        for doc in res:
            fp = ip = ts = pageid = url = td = ''
            fp = doc['_source']['fp']
            ip = doc['_source']['geoip']['ip']
            path = doc['_source']['path']
            ts = doc['_source']['updated']
            pageid = doc['_source']['pageid']
            url = doc['_source']['url']
            ti = ckattr(doc['_source'], 'title')
            # ti = doc['_source'].get('title', '')
            # aimtuple = (fp, ip, ts, path, pageid, url)
            # aimtuple = (fp, ip, ts,)
            # logger.info(aimtuple)
            # continue
            if ts == eachts:  # same as candidate, skip!
                continue
            doctsObj = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S')
            if tObj > doctsObj:
                if path == path_param:
                    eachDict['bself'] += 1
                td = dtdelta2min(tObj - doctsObj)  # the fourth in tuple
                aimtuple = (fp, ip, ts, td, path, pageid, url, ti)
                eachDict['prev'].append(aimtuple)
            elif tObj < doctsObj:
                if path == path_param:
                    eachDict['aself'] += 1
                td = dtdelta2min(doctsObj - tObj)  # the fourth in tuple
                aimtuple = (fp, ip, ts, td, path, pageid, url, ti)
                eachDict['next'].append(aimtuple)
            count += 1
            # if count == 1:
            #     break
        # print(str(tObj), eachDict)
        eachDict['prev'] = sorted(
            eachDict['prev'], key=itemgetter(2), reverse=True)
        eachDict['next'] = sorted(eachDict['next'], key=itemgetter(2))
        # logger.info('num of processed doc for ' +
        #             str(keytuple) + ': ' + str(count))
        # logger.info('num of prev doc: ' + str(len(eachDict['prev'])))
        # logger.info('num of next doc: ' + str(len(eachDict['next'])))
        # retList.append(eachDict)
    # return retList
    return retDict


def multiproc():
    pass
    global q_list

    procc = proc if proc <= len(q_list) else len(q_list)
    with mp.Pool() as pool:
        res = pool.map(worker, range(procc))

    # merge dicts
    joinDict = {}
    for eachdict in res:
        # print(eachdict)
        joinDict.update(eachdict)

    # simple stat
    # for k, il in joinDict.items():
    #     print(k, str(len(il)))

    # print(joinDict)
    # outfile = 'data/' + label + '.csv'
    # with open(outfile, 'w') as csvf:
    #     writer = csv.writer(csvf)
    #     writer.writerow(['path', 'fp', 'ip', 'ts'])
    #     for k, il in joinDict.items():
    #         for it in il:
    #             outlist = []
    #             outlist.append(k)
    #             outlist.append(it[0])
    #             outlist.append(it[1])
    #             outlist.append(it[2])
    #             writer.writerow(outlist)

    outDict = {}
    for k, il in joinDict.items():
        procc = proc if proc <= len(il) else len(il)
        q_list = il
        global path_param
        path_param = k
        with mp.Pool() as pool:
            res = pool.map(getExtendedFpInfo, range(procc))
            bigDict = {}
            for eachDict in res:
                bigDict.update(eachDict)
            # for ik, iv in bigDict.items():
            #     print(k, ik, len(iv['prev']), len(iv['next']))  # should be tuple
            outDict[k] = bigDict

    outfile1 = 'data/' + label + '_ui.csv'  # ui stands for user info
    outfile2 = 'data/' + label + '_ub.csv'  # ub stands for user behavior
    outfile3 = 'data/' + label + '_uo.csv'  # uo stands for user openlink
    header1 = ['path', 'candidate', 'ol_count', 'gender',
               'age', 'before', 'after', 'bself', 'aself']
    header2 = ['path', 'candidate', 'gender', 'age',
               'type', 'datetime', 'mindiff', 't_path', 't_pageid', 't_url', 't_title']
    header3 = ['path', 'candidate', 'ol_external', 'ol_href', 'ol_text']
    types = ['prev', 'next']
    with open(outfile1, 'w') as out1, open(outfile2, 'w') as out2, open(outfile3, 'w') as out3:
        writer1 = csv.writer(out1)
        writer1.writerow(header1)
        writer2 = csv.writer(out2)
        writer2.writerow(header2)
        writer3 = csv.writer(out3)
        writer3.writerow(header3)
        for cpath, eachDict in outDict.items():
            for ktuple, kdict in eachDict.items():
                sex = kdict['sex']
                age = kdict['age']
                ol_count = len(kdict['openlink'])
                # ol_len, external, href, text = kdict['openlink']
                outlist1 = []
                outlist1 = [i for i in [cpath, str(ktuple), str(ol_count), sex, age, str(
                    len(kdict['prev'])), str(len(kdict['next'])), str(kdict['bself']), str(kdict['aself'])]]
                writer1.writerow(outlist1)
                if ol_count > 0:
                    for ituple in kdict['openlink']:
                        external = ituple[0]
                        href = ituple[1]
                        text = ituple[2]
                        outlist3 = []
                        outlist3 = [i for i in [cpath, str(
                            ktuple), external, href, text]]
                        writer3.writerow(outlist3)
                for itype in types:
                    for ttuple in kdict[itype]:
                        fp, ip, ts, td, path, pageid, url, ti = ttuple
                        outlist2 = []
                        outlist2 = [eachi for eachi in [cpath, str(
                            ktuple), sex, age, itype, ts, td, path, pageid, url, ti]]
                        writer2.writerow(outlist2)


if __name__ == '__main__':
    multiproc()
    logger.info('Done!')
