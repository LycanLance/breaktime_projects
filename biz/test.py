# -*- coding: utf-8 -*-
from urllib.parse import urlencode, quote_plus, quote

# payload = {'username':'administrator', 'password':'xyz'}
# result = urlencode(payload, quote_via=quote_plus)

a = u"*日立*"
print(a)
print(type(a))

# some_string.encode(encoding)
b = "*日立*"
b = b.encode('utf-8')
print(b)
print(type(b))

c = {"query":"日立"}
c = urlencode(c)
print(c)
print(type(c))

d = "日立"
d = quote(d)
print(d)
print(type(d))