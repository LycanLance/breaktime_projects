#!/root/anaconda3/envs/my_env/bin/python

import pandas as pd
import sys
# import os
# import csv
# import urllib.request
from hanziconv import HanziConv

# df =  pd.read_csv('http://data.ntpc.gov.tw/od/zipfiledl?oid=A7F473A4-A633-4D5A-8DA7-CB1E62597A08&ft=zip')
# v2, add params as conf file
# conf format:
# csv,col
# v3, process the file directly from url
# failed to download file from data.ntpc.gobv.tw
# v4, get file from https://quality.data.gov.tw/dq_download_csv.php?nid=55337&md5_url=43ea6ebf73387810168c47b897b8e1ab

conf = sys.argv[1]
# df = pd.read_csv('http://www.twse.com.tw/company/suspendListingCsvAndHtml?type=csv&lang=zh', encoding="cp1252")
# print(df.head())
# sys.exit()

for aline in open(conf, 'r'):
	line = aline.strip()
	if not line.startswith("#"):
		# print(line)
		# continue
		param = line.split(",")
		infile = param[2]
		filename = param[0]
		col = param[1]
		# print([filename,col,infile])
		# continue
		outfile = filename+'_'+col+'_p.txt'
		df = pd.read_csv(infile)
		# sys.exit()
		df = df.drop_duplicates(subset=col)
		df = df[pd.notnull(df[col])].reset_index()[col]
		# print(type(df))
		df= df.to_frame() # important: turn series to dataframe, 
		# print(df.head)
		# break
		# print(type(df))
		# print(df)

		tmplist = []
		for i in range(len(df)):
			# pass
			tmplist.append(HanziConv.toSimplified(df.loc[i, col]))

		# print(type(tmplist))

		df.loc[:,'sc'] = pd.Series(tmplist, index=df.index) # assign a list to a new column in the dataframe
		# print(df)
		# tmpdict = {'sc': tmplist} # works!
		# df = pd.DataFrame(tmpdict, columns = ['sc'])

		# print(df)

		df['sc'].to_csv(outfile, header=False, index=False)
	else:
		continue
