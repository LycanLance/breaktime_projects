#!/root/anaconda3/envs/my_env/bin/python

import pandas as pd
import sys
import os
import csv

# df =  pd.read_csv('http://data.ntpc.gov.tw/od/zipfiledl?oid=A7F473A4-A633-4D5A-8DA7-CB1E62597A08&ft=zip')

# v2, add params as conf file
# conf format:
# csv,col

conf = sys.argv[1]

linelist = []
file = open(conf, 'r')
# print(type(file))
for aline in file:
	linelist.append(aline.strip())

for line in linelist:
	param = line.split(",")
	infile = param[0]
	col = param[1]
	filename = os.path.splitext(infile)[0]
	outfile = filename+'_parsed.csv'

	df = pd.read_csv(infile)
	df = df[pd.notnull(df[col])]
	name = df[col]
	name.to_csv(outfile, index=False)

